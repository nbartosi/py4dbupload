#!/usr/bin/env python
# $Id$
# Created by Stefan Maier <s.maier@cern.ch>, 31-May-2024

# This script masters the registration of the 2S Module components.

import os,traceback,sys,time

from AnsiColor   import Fore, Back, Style
from DataReader  import TableReader,scale,scale2
from Sensor      import *
from Utils       import *
from optparse    import OptionParser
from decimal     import Decimal, ROUND_HALF_UP
from progressbar import *
from OT2SModule  import OT2SModule

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the CSV file with component data')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')

   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')

   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database.')

   p.add_option( '--store',
               action  = 'store_true',
               default = False,
               dest    = 'store',
               help    = 'Store the generated XML/ZIP file. Otherwise, delete it immediately')

   (opt, args) = p.parse_args()

   if len(args)>2:
      p.error("accepts at most one argument!")

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'
   
   module_2S_file  = opt.data_path

   files_to_be_uploaded = []

   module_2S_component_reader      = TableReader(module_2S_file, d_offset=0, csv_delimiter=',', tabSize=20)

   Module_2S_Container = UploaderContainer(os.path.dirname(module_2S_file) + '/' +'OT2SModule')

   print ('\n\Components of Module to upload',   module_2S_component_reader)

   #Get the three data components from the csv file
   SMMRY = OT2SModule( module_2S_component_reader)
   Module_2S_Container.add(SMMRY)
   time.sleep(0.1)

   streams.flush()
   files_to_be_uploaded.append(Module_2S_Container.dump_xml_data(False))

   # Upload files in the database 
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=True)
   if opt.upload:  
      for fi in files_to_be_uploaded:
         # Files upload
         db_loader.upload_data(fi)

   if not opt.store:
      for fi in files_to_be_uploaded:
         print("Remove temporary files")
         os.remove(fi)
