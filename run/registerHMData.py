#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 19-Feb-2021

# This script masters the registration of the Halfmoon Condition Data.

import os,traceback,sys

from AnsiColor   import Fore, Back, Style
from DataReader  import TableReader,scale,scale2
from Sensor      import *
from Utils       import *
from optparse    import OptionParser
from decimal     import Decimal, ROUND_HALF_UP
from progressbar import *
from Exceptions  import NotRecognized

from os          import path
import sys


def load_halfmoon_data(hm_id,container,run_conf,data_conf,data):
   """Loads data into the container, creates the metadata if needed."""
   # Creates the data uploader
   uploader = HalfmoonMeasurements(run_conf,data_conf,halfmoon_id=hm_id,dreader=data)

   # Check if Metadata exist for the same flute and structure
   for meta in container.uploaders:
      if meta.flute()==uploader.flute() and meta.structure()==uploader.structure():
         meta.add_child(uploader)
         return

   meta_conf = {
                 'data_version'     : 'v1',
                 'data_comment'     : 'metadata with flute and structure.',
               }

   meta = HalfmoonMeasurements(run_conf,meta_conf, data_uploader=uploader)
   meta.add_child( uploader )
   container.add( meta )



if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <halfmoon id>", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with measurement data.')
   p.add_option( '-l','--location',
               type    = 'string',
               default = 'CERN',
               dest    = 'location',
               metavar = 'STR',
               help    = 'Specify where the measurement run have been made.')
   p.add_option( '-o','--operator',
               type    = 'string',
               default = 'A. Di Mattia',
               dest    = 'operator',
               metavar = 'STR',
               help    = 'Specify the operator that made the measurements.')
   p.add_option( '--date',
               type    = 'string',
               default = '2021-02-19',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Specify the date (format: YYYY-MM-DD) of the measurements.')
   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')



   (opt, args) = p.parse_args()

   if len(args)!=1:
      p.error("accepts only one argument!")


   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'

   # write the version and the description for the IV data
   iv_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test',
             }

   # write the version and the description for the CV data
   cv_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_2',
             }

   # write the version and the description for the diode cv data
   diodecv_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_3',
                    }

   # write the version and the description for the igv data
   igv_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_4',
                  }

   # write the version and the description for the tc data
   tc_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_5',
                  }

   # write the version and the description for the tc data
   mask_mis_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_6',
                  }

   # write the version and the description for the tc data
   mask_mis_par_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_7',
                  }

   # gather excel files conatining the data measurements
   data_files = search_files(opt.data_path,'*.txt')

   conditions = UploaderContainer('ConditionData')

   runp = RunProvider(use_database=False)  # set to True for extracting run from
                                           # CMSR Database
   runp.setDbQuery('select r.run_number from trker_cmsr.trk_ot_test_nextrun_v r')
   #runp.setDbQuery('select r.run_number from trker_int2r.trk_ot_test_nextrun_v r')


   # Retrieve the FE Hybrid not associated to CBC
   nFiles = len(data_files)
   bar = progressbar(range(nFiles), widgets=['Processing Halfmoon data files: ', \
                                       Bar('=', '[', ']'), ' ', Percentage()] )

   for i in bar:
      f = data_files[i]
   #for i,f in enumerate(data_files):
      data = TableReader(f, tabSize=14, tabNumber=5, csv_delimiter=",")
      data_name = path.splitext( path.basename(data.filename) )[0]
      data_type = data_name.split('-')[2]


      data_conf = {}
      if data_type=='IV':       data_conf.update(iv_data)
      if data_type=='CV':       data_conf.update(cv_data)
      if data_type=='DIODECV':  data_conf.update(diodecv_data)
      if data_type=='IGV':      data_conf.update(igv_data)
      if data_type=='TC':       data_conf.update(tc_data)
      if data_type=='MM':       data_conf.update(mask_mis_data)
      if data_type=='MMP':      data_conf.update(mask_mis_par_data)

      # Extract new run number for every measurement 
      run_conf = {
         'run_type'         : 'PQC',
         'run_number'       : runp.run_number(),
         'run_comment'      : 'test for development',
         'run_operator'     : opt.operator,
         'run_begin'        : opt.date,
         'run_location'     : opt.location
                 }

      try:
         load_halfmoon_data(args[0],conditions,run_conf,data_conf,data)
      except Exception as e:
         print( e )
         for u in conditions.uploaders: print(u)
         sys.exit(1)

      #bar.update(i)


   conditions.dump_xml_zip()
