#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 28-March-2024

# This script masters the registration of the IT Sensor components.

import os,traceback,sys,time

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader,scale,scale2
from ITSensor     import ITwafer, ITsensor, ITsensorMeasurements, IThalfmoon,\
                         IThalfmoonMeasurements, ITDataType, ITSensType,\
                         ITHRegions
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,search_files
from optparse     import OptionParser
from copy         import deepcopy
from decimal      import Decimal, ROUND_HALF_UP
from progressbar  import *


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with measurement data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   (opt, args) = p.parse_args()

   if len(args)>2:
      p.error("accepts at most one argument!")


   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'

   # write the description and the attributes for all the Wafer components
   wafer_conf = {
      'description'      : '', # write the user comment on the component here
      'attributes'       : [('Status','Good')],
   }

   # write the description and the attributes for all the Sensor components
   sensor_conf = {
      'description'      : '',# write the user comment on the component here
      'attributes'       : [('Status','Good')],
   }

   # write the version and the description for the IV data
   iv_data = {
      'data_version'     : 'v1',
      'data_comment'     : '',    # write the user comment on the dataset here
             }

   # write the version and the description for the CV data
   cv_data = {
      'data_version'     : 'v1',
      'data_comment'     : '',    # write the user comment on the dataset here
             }

   # write the version and the description for the summary data
   summary_data = {
      'data_version'     : 'v1',
      'data_comment'     : '',    # write the user comment on the dataset here
                  }

   # gather excel files conatining the data measurements
   data_files = sorted( search_files(opt.data_path,'*.txt') )

   wafers       = UploaderContainer('Wafers')
   sensors      = UploaderContainer('Sensors')
   sensorData   = UploaderContainer('SensorData')
   halfmoons    = UploaderContainer('Halfmoons')
   halfmoonData = UploaderContainer('HalfmoonData')

   envelope_ids = None
   if len(args)!=0:  envelope_ids = TableReader(args[0])

   nFiles = len(data_files)
   bar = progressbar(range(nFiles), widgets=['Processing Hamamatsu files: ', \
                Bar('=', '[', ']'), ' ', Percentage()])

   for i in bar:
      f = data_files[i]
      sd = TableReader(f, d_offset=1, m_rows=25,tabSize=43,csv_delimiter="\t")
      iv = TableReader(f, d_offset=26,m_rows=82,tabSize=15,csv_delimiter="\t")
      cv = TableReader(f, d_offset=109,m_rows=42,tabSize=15,csv_delimiter="\t")
      
      # store wafer classes for registration
      wafer = ITwafer(wafer_conf,sd,envelope_ids)
      wafers.add(wafer)

      # store sensor / halfmoon classes for registration
      sensor_type = sd.getDataFromTag('#Sensor Type')
      try:
         ITSensType[str(sensor_type)]  # check if type is an Halfmoon cut
         sensor = ITsensor(sensor_conf,sd,envelope_ids)
         sensors.add(sensor)
      except:
         halfmoon = IThalfmoon(sensor_conf,sd,None,envelope_ids)
         halfmoons.add(halfmoon)

      # set run number and run comment
      run_data = {
         'run_type'         : 'VQC',
         'run_comment'      : '',    # put the user comment on the run here
                }

      # store sensor / halfmoon measurements
      try:
         ITSensType[str(sensor_type)]  # check if type is an Halfmoon cut
         IV = ITsensorMeasurements(run_data,iv_data,ITDataType.iv,iv,sd)
         CV = ITsensorMeasurements(run_data,cv_data,ITDataType.cv,cv,sd)
         SMMRY = ITsensorMeasurements(run_data,summary_data,ITDataType.summary,cv,sd)
         #SMMRY.attach_part = False
         SMMRY.add_child( IV )
         SMMRY.add_child( CV )
         sensorData.add(SMMRY)
      except:
         IV = IThalfmoonMeasurements(run_data,iv_data,ITDataType.iv,iv,sd)
         CV = IThalfmoonMeasurements(run_data,cv_data,ITDataType.cv,cv,sd)
         SMMRY = IThalfmoonMeasurements(run_data,summary_data,ITDataType.summary,cv,sd)
         #SMMRY.attach_part = False
         SMMRY.add_child( IV )
         SMMRY.add_child( CV )
         halfmoonData.add(SMMRY)
         
      time.sleep(0.1)

   streams.flush()

   halfmoons_without_file=[]
   for wafer in wafers.names():
      for region in ITHRegions:
         hname = f'{wafer}_{region.name}'
         if hname not in halfmoons.names():
            halfmoons_without_file.append(hname)
   
   nHalfmoons = len(halfmoons_without_file)
   bar = progressbar(range(nHalfmoons), widgets=['Processing Halfmoons with no file: ', \
                Bar('=', '[', ']'), ' ', Percentage()])
   for i in bar:
      n = halfmoons_without_file[i]
      conf = deepcopy(sensor_conf)
      conf['name'] = n
      if any(n in s for s in halfmoons.names()):
         conf['attributes'].append(('IT Halfmoon Structure','Sub Cut'))
      else:
         conf['attributes'].append(('IT Halfmoon Structure','Full'))
      
      wafer_name = '_'.join(n.split('_')[:4])
      halfmoon = IThalfmoon(conf,None,wafers.find(wafer_name),)
      halfmoons.add(halfmoon)
   
      time.sleep(0.1)
   
   streams.flush()
   
   wafer_xml  = wafers.dump_xml_data()
   sensor_xml = sensors.dump_xml_data(wafers.uploaders)
   qcdata_xml = sensorData.dump_xml_zip()
   halfmn_xml = halfmoons.dump_xml_data(wafers.uploaders)
   hmdata_xml = halfmoonData.dump_xml_zip()
   
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                      login_type=BaseUploader.login,verbose=opt.debug)
   
   if opt.upload:
      # The test mode of the DB-Loader does not work for attributes 
      db_loader.upload_data(wafer_xml)
      db_loader.upload_data(sensor_xml)
      db_loader.upload_data(qcdata_xml)
      db_loader.upload_data(halfmn_xml)
      db_loader.upload_data(hmdata_xml)