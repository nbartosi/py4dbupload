from Query import Query
from optparse     import OptionParser
import json

if __name__ == "__main__":
    p = OptionParser(usage="usage: %prog [options]", version="1.0")

    p.add_option( '--dev',
                    action  = 'store_true',
                    default = False,
                    dest    = 'isDevelopment',
                    help    = 'Set the development database as target.')

    p.add_option( '--verbose',
                    action  = 'store_true',
                    default = False,
                    dest    = 'verbose',
                    help    = 'Force the uploaders to print their configuration and data.')

    p.add_option( '--locations',
                    action  = 'store_true',
                    default = False,
                    dest    = 'locations',
                    help    = 'Print all locations stored in the DB')
    
    p.add_option( '--testConditions2s',
                    action  = 'store_true',
                    default = False,
                    dest    = 'testConditions2s',
                    help    = 'Get table of test conditions for 2S modules')

    p.add_option( '--testConditionsPs',
                    action  = 'store_true',
                    default = False,
                    dest    = 'testConditionsPs',
                    help    = 'Get table of test conditions for PS modules')
    
    p.add_option( '--testConditionsSkeleton',
                    action  = 'store_true',
                    default = False,
                    dest    = 'testConditionsSkeleton',
                    help    = 'Get table of test conditions for 2S skeletons')

    p.add_option( '--otModuleInformation',
                    type    = 'string',
                    default = False,
                    dest    = 'otModuleInformation',
                    metavar = 'STR',
                    help    = 'Get OT module information')

    p.add_option( '--metrology',
                    type    = 'string',
                    default = None,
                    dest    = 'metrology',
                    metavar = 'STR',
                    help    = 'Get metrology results of a module')
    
    p.add_option( '--sensorMetrology',
                    type    = 'string',
                    default = None,
                    dest    = 'sensor_metrology',
                    metavar = 'STR',
                    help    = 'Get metrology results of a sensor')

    p.add_option( '--moduleIV',
                    type    = 'string',
                    default = None,
                    dest    = 'moduleIV',
                    metavar = 'STR',
                    help    = 'Get IV results of a module')
    p.add_option( '--moduleRootFileList',
                    type    = 'string',
                    default = None,
                    dest    = 'otModuleRootFile',
                    metavar = 'STR',
                    help    = 'Get List of root file entries of a module')

    p.add_option( '-t','--test',
                    action  = 'store_true',
                    default = False,
                    dest    = 'test',
                    help    = 'Test')

    p.add_option( '--sensorIV',
                    type    = 'string',
                    default = None,
                    dest    = 'sensorIV',
                    metavar = 'STR',
                    help    = 'Get IV results of a sensor')

    p.add_option( '--boxes',
                    action  = 'store_true',
                    default = False,
                    dest    = 'boxes',
                    help    = 'Get location of boxes')


    p.add_option( '-l','--last',
                    action  = 'store_true',
                    default = False,
                    dest    = 'last',
                    help    = 'Only get last entry')

    p.add_option( '-p','--pipe',
                    type    = 'string',
                    default = None,
                    dest    = 'pipe',
                    metavar = 'STR',
                    help    = 'Named pipe to forward data to')

    p.add_option( '--2fa',
                action  = 'store_true',
                default = False,
                dest    = 'twofa',
                help    = 'Set the two factor authentication login.')

    (opt, args) = p.parse_args()

    db = 'trker_cmsr'
    if opt.isDevelopment:
        db = 'trker_int2r'
        
    query = Query(db,'login' if not opt.twofa else 'login2', opt.verbose)

    if opt.pipe:
        named_pipe = opt.pipe

    data = None
    if opt.locations:
        data = query.get_locations()

    if opt.metrology:
        data = query.get_ot_module_metrology(opt.metrology,opt.last)
    
    if opt.sensor_metrology:
        data = query.get_ot_sensor_metrology(opt.sensor_metrology,opt.last)

    if opt.moduleIV:
        data = query.get_ot_module_iv(opt.moduleIV,opt.last)

    if opt.sensorIV:
        data = query.get_ot_sensor_iv(opt.sensorIV,opt.last)

    if opt.testConditions2s:
        data = query.get_ot_test_conditions_2s(opt.last)

    if opt.testConditionsPs:
        data = query.get_ot_test_conditions_ps(opt.last)

    if opt.testConditionsSkeleton:
        data = query.get_ot_test_conditions_skeleton(opt.last)

    if opt.otModuleInformation:
        if "2S" in opt.otModuleInformation:
            data = query.get_ot_module_information_2s(opt.otModuleInformation)
        elif "PS" in opt.otModuleInformation:
            data = query.get_ot_module_information_ps(opt.otModuleInformation)
    if opt.otModuleRootFile:
        data = query.get_ot_module_root_file_list(opt.otModuleRootFile, opt.last)
    if opt.boxes:
        data = query.get_ot_boxes()
    if opt.test:
        data = query.test()


    if opt.pipe:
        with open(named_pipe, "w") as pipe:
            pipe.write(json.dumps(data))
    else:
        [print(dataPoint) for dataPoint in data]
