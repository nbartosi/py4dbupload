#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 29-Nov-2022

# This script masters the registration of the SSA Chips and Wafers components.


import os,sys,time

from DataReader   import TableReader
from SSA          import SSACICwafer,SSAchip
from Utils        import search_files, UploaderContainer, DBupload
from BaseUploader import BaseUploader
from datetime     import date
from dateutil     import parser
from optparse     import OptionParser
from progressbar  import *



if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] [cvs table file]", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the csv tables with SSA wafer data.')

   p.add_option( '--date',
               type    = 'string',
               default = '',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component production; overwitten by the date in the cvs table.')

   p.add_option( '-v','--ver',
               type    = 'string',
               default = '1.0',
               dest    = 'ver',
               metavar = 'STR',
               help    = 'Version type of the components; overwitten by the tag in the csv table.')

   p.add_option( '-o','--operator',
               type    = 'string',
               default = '',
               dest    = 'operator',
               metavar = 'STR',
               help    = 'The operator that performed the data registration')

   p.add_option( '--dev',
                  action  = 'store_true',
                  default = False,
                  dest    = 'isDevelopment',
                  help    = 'Set the development database as target.')
   
   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the upload of the XML file.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')


   (opt, args) = p.parse_args()


   if len(args)>1:
      p.error("accepts at most 1 argument!")


   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'
   
   # Getting the part data from database
   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'

   date_of_production = parser.parse(opt.date) if opt.date!='' else date.today()
   
   
   wafers = UploaderContainer('wafers')
   chips  = UploaderContainer('SSAchips')


   # gather ssa wafer csv data file
   ssa_files = []
   if len(args)==1:  ssa_files.append( args[0] )
   else:             ssa_files = sorted( search_files(opt.data_path,'*.csv') )

   files_to_be_registered = []

   #process csv wafer files 
   for file in ssa_files:
      ssa_data = TableReader(file,csv_delimiter=',',tabSize=13)
      data     = ssa_data.getDataAsCWiseDictRowSplit()
      nItems   = len(data) + 1
      bar = progressbar(range(nItems), widgets=[f'Processing {ssa_data.filename}: ', \
             Bar('=', '[', ']'), ' ', Percentage()])


      for i in bar:
         if (i==0):  # Process the wafer
            # write the description and the attributes for the Wafer component
            # version is not used for Wafers as they may have different chips types
            wafer_conf = {
               'product_date'     : date.strftime(date_of_production,'%Y-%m-%d'),
               #'description'      : 'Uncomment this for reporting something on the component',
            }
            wafer = SSACICwafer(wafer_conf,data[0])
            wafers.add(wafer)
            
         else:      # Process the chips
            # write the description and the attributes for all the Sensor components
            chip_conf = {
               'version'          : opt.ver,
               'product_date'     : date.strftime(date_of_production,'%Y-%m-%d'),
               #'description'      : 'Uncomment this for reporting something on the component',
            }
            try:
               chip = SSAchip(chip_conf,data[i-1])
               chips.add(chip)
            except:
               raise
           
         time.sleep(0.1)

   print('\n')
   streams.flush()
   
   # Finds duplicates in serial numbers and name_labels
   serials = []
   ser_dup = []
   for chip in chips.uploaders:
      if chip.serial()!='':
         if chip.serial() not in serials:  serials.append(chip.serial())
         else: ser_dup.append(chip.serial())
      
   if len(ser_dup)!=0:  
      print(f'list of serial numbers duplicated:')
      print(ser_dup)
      sys.exit(1)
   
   
   
   files_to_be_registered.append(wafers.dump_xml_data())
   files_to_be_registered.append(chips.dump_single_xml_data(wafers.uploaders))

   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                        login_type=BaseUploader.login,verbose=True)
   if opt.upload:  
      for fi in files_to_be_registered:
         # Files upload
         db_loader.upload_data(fi)
