#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 23-Mar-2019
# Modified by Alessandro Di Mattia <dimattia@cern.ch>, 04-Jun-2020
#  + implementation of the new naming convention for the serial numbers

# This module contains the classes that produce the XML file for uploading
# the sensor, the sensor wafers and the sensor QC data.

from BaseUploader import BaseUploader,ComponentType, ConditionData
from DataReader   import scale2, consistency_check
from lxml         import etree
from time         import gmtime, strftime
from Utils        import check_ot_module_label

def getTupleEntry(pTupleList, pKey):
   for tuple in pTupleList:
      if tuple[0] == pKey:
         if tuple[2]!="":
            entry1 = str(tuple[1])
            entry2 = str(tuple[2])
            return entry1, entry2
         else:
            entry = str(tuple[1])
            return entry

class OT2SModule(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the OT Wafer
      component data read from the txt file shipped by Hamamatsu."""

   name = ""
   def __init__(self, reader):
      """Constructor: 
      """
      file_data = reader.getDataAsRWiseVector()

      cdict = {}
      self.name,self.status = getTupleEntry(file_data,"NAME_LABEL")
      if not check_ot_module_label(self.name):
         print("Error - Module name not compliant with naming convention!")
         exit(1)
      else:
         print("Module naming convention: checked")
      cdict['kind_of_part']   = '2S Module'
      cdict['manufacturer']   = getTupleEntry(file_data,"MANUFACTURER")
      cdict['unique_location']= getTupleEntry(file_data,"LOCATION")
      cdict['name_label']     = self.name
      cdict['barcode']        = self.name
      cdict['serial']         = self.name

      spacing = self.get_sensor_spacing()
      contact_points = self.get_contact_points()

      cdict['attributes']     = [["Status",self.status],["2S Sensor spacing",spacing], ["Contact Points",contact_points]]

      sensor_top_id, rotation_top= getTupleEntry(file_data,"SENSOR_TOP")
      sensor_bot_id, rotation_bottom= getTupleEntry(file_data,"SENSOR_BOTTOM")
      feh_r_id= getTupleEntry(file_data,"FEH_RIGHT")
      feh_l_id= getTupleEntry(file_data,"FEH_LEFT")
      seh_id= getTupleEntry(file_data,"SEH")
      vtrx_id= getTupleEntry(file_data,"VTRX+")

      cdict['children']       = [["2S Sensor", sensor_top_id,{"attributes":[["2S Sensor Posn","TOP"],["Sensor Orientation",rotation_top]]}],
                                 ["2S Sensor", sensor_bot_id,{"attributes":[["2S Sensor Posn","BOTTOM"],["Sensor Orientation",rotation_bottom]]}],
                                 ["2S Front-end Hybrid", feh_r_id],
                                 ["2S Front-end Hybrid", feh_l_id],
                                 ["2S Service Hybrid", seh_id],
                                 ["VTRx+",vtrx_id]]
      BaseUploader.__init__(self, reader=reader,configuration=cdict)
      self.mandatory += ['kind_of_part','unique_location','manufacturer', "name_label"]#,\
      #                    'attributes']
      "Check if location matches"
      child_ids = [child[1] for child in cdict['children']]
      self.do_checks(child_ids,cdict['unique_location'])

   def barcode(self):
      """Returns the list_of_ids."""
      return self.name

   def serial(self):
      """Returns the list_of_ids."""
      return self.name

   def get_contact_points(self):
      return self.name[6]
   def get_sensor_spacing(self):
      return self.name[3] + "." + self.name[4] +" mm"


   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")
      self.xml_builder(parts)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def xml_builder(self, parts, *args):
      self.build_parts_on_xml(parts, name_label=self.retrieve("name_label"),serial=self.retrieve("serial"),barcode=self.retrieve("barcode"))#,extended_data=ex)

   def do_checks(self, component_ids,location):
      #Check Block
      location_id_of_location = self.db.get_location_id(location)
      #Check if the location name is valid
      if location_id_of_location is None:
         print("Error - location does not exist")
         exit(1)
      else:
         print("Location: checked")
      print(self.db.get_parent_id("2S_18_5_KIT-11111"))

      for component_id in component_ids:
         print("Check", component_id)

         #Check if the component exists in the DB
         if self.db.component_id(component_id,idt='serial_number') is None:
            print("Error - Component not in DB!")
            exit(1)
         else:
            print("Component id: checked")

         #Avoid overwriting relations by checking the parent_id
         if self.db.get_parent_id(component_id) not in [1000,None]:
            print("Error - Component already has parent. Use DCA to make changes!")
            exit(1)
         else:
            print("No relation of component: checked")


         #Check if the measurement is at the same location as the component is
         location_id_of_component = self.db.component_location(component_id)
         if location_id_of_location != location_id_of_component:
            print("Error - Component to be linked does not match location of site")
            exit(1)
         else:
            print("Location of component matches: checked")
