#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 25-May-2022

# This module contains the classes that produce the XML file for uploading
# the PS Baseplate components.

from BaseUploader import BaseUploader, ConditionData
from lxml         import etree
from shutil       import copyfile
from copy         import deepcopy
from Exceptions   import *
from Utils        import search_files
from enum         import Enum
from datetime     import datetime
import sys


class Baseplate():
   """Class to decode the serial number given to the Baseplate components (panel and plate)."""
   
   def __init__(self, serial):
      """Constructor: it requires the serial number that is used to identify the component."""
      self.serial = serial
      
      try:
         self.b_id = serial.split('_')[1]
      except:
         self.b_id = None
      
      self.b_kind = 'CF 3-ply Panel' if self.b_id==None else 'PS Baseplate'
      
      self.name_label = f'CF3plyP_{serial}' if self.b_id==None else f'PSBP_{serial}'
      
      
      
class BaseplateComponentT(Baseplate,BaseUploader):
   """Produces the XML file to register any baseplate components.""" 

   def __init__(self, serial, location, manufacturer, date, comment=None, inserter=None):
      """Constructor for component registration: it requires the serial number 
         of the baseplate component, the component location, the manufacturer 
         and the production date to produce the configuration dictionary. 
            Optional parameneters are:
            comment:      description of the component.
           inserter:     person recording the data (override the CERN user name)
       """
      Baseplate.__init__(self,serial)

      name = self.name_label
      cdict = {}
      self.children   = []
      
      cdict['kind_of_part']    = self.b_kind      
      cdict['unique_location'] = location 
      cdict['manufacturer']    = manufacturer
      cdict['product_date']    = date
      cdict['serialNumber']    = self.serial
      cdict['nameLabel']       = self.name_label
      # cdict['version']         = 'Empty'    Should we define a version string?

      if comment!=None: cdict['description'] = comment
      if inserter!=None: cdict['inserter'] = inserter

      BaseUploader.__init__(self, name, cdict)
      
      self.mandatory += ['manufacturer','kind_of_part','unique_location']


   def add_children(self,child):
      """Add a child."""
      if self.b_kind=='CF 3-ply Panel' and child.b_kind=='PS Baseplate':
         self.children.append(child)

   
   def load_attribute(self, name, value):
      """Load a component attribute. Check the allowed attributes."""
      try:
         allowed = {}
         for a in self.attributes:
            allowed[a['attributeName']] = a['allowedValues'].split(',')
      except:
         allowed = None
            
      if allowed!=None:
         if name not in allowed.keys():
            raise BadParameters(self.__class__.__name__,f'{name} is not an attribute of {self.b_kind}.')
      
         allowed_values = allowed[name]
         if value not in allowed_values:
            raise BadParameters(self.__class__.__name__,f'{value} is not allowed for {name} attribute.')
      
      attributes = self.retrieve('attributes')
      
      try:
         set_attributes = [a for a in attributes if a[0]!=name]
      except:
         set_attributes = []
         
      set_attributes.append((name,value))
      self.configuration['attributes']=set_attributes


   def xml_builder(self,parts):
      """Process data."""
      part = None
      if self.b_kind=='CF 3-ply Panel':
         from Utils import DBaccess
         #db = DBaccess(database=f'trker_{self.database}')
         part_id = self.db.component_id(self.name_label,kop=self.retrieve('kind_of_part'))
         if part_id!=None:
            # registered
            part = etree.SubElement(parts, "PART", mode="auto")
            etree.SubElement(part,"KIND_OF_PART").text = self.b_kind
            etree.SubElement(part,"SERIAL_NUMBER").text = self.serial
         else:
            # not registered
            part = self.build_parts_on_xml(parts,serial=self.serial,name_label=self.name_label)
      else:
         part = self.build_parts_on_xml(parts,serial=self.serial,name_label=self.name_label)
      
      if len(self.children)!=0:
         children = etree.SubElement(part,"CHILDREN")
         for child in self.children:
            child.xml_builder(children)


   def dump_xml_data(self, filename=''):
      """Writes the hybrid data in the XML file for the database upload."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree)

      if filename == '':
         filename = '%s.xml'%self.name
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                        xml_declaration = True,\
                                        standalone="yes").decode('UTF-8') )
      return filename
