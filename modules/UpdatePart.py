#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 30-Nov-2019

# This module contains the classes that produce the XML file for updating the
# parts in the database.

from AnsiColor    import Fore, Back, Style
from copy         import deepcopy
from BaseUploader import BaseUploader
from Utils        import IdType, clear
from lxml         import etree
from enum         import Enum
from Exceptions   import NotRecognized,MissingParameter

import re,os,sys,time

Mode = Enum('Mode','basic expert batch')

class Update(BaseUploader):
   """Class to produce the xml file for updating the parts inserted in the database.
   """
   
   expert_att = { 'PS Front-end Hybrid' : ['FE Hybrid Side','PS Sensor spacing'],
                }
   
   def __init__(self, id, Idt='serial_number', default={}, inserter=None,\
                mode='basic',debug=False):
      """Constructor: it requires the component id (id), the type of id (idt),
         the default configuration (default), the shadow operator (inserter), 
         the operation mode and the debug Flag to activate the printout of the 
         database access queries.
      """
      conf = deepcopy(default)
      try:
         self.mode = Mode[str(mode)]
      except:
         modes = [m.name for m in Mode]
         print(Fore.RED+f'{mode}'+Style.RESET_ALL+' is not a valid mode!')
         print('valid modes are: '+Fore.BLUE+f'{modes}'+Style.RESET_ALL)
         exit(1)
      
      # Configure the class name as the component id
      BaseUploader.__init__(self, id, configuration=conf)
      self.mandatory = ['kind_of_part']
      self.inserter  = inserter
      self.children  = []
      
      if self.mode.name!='batch':   
         clear()
         print('Gathering data for '+Fore.BLUE+f'{id} ...'+Style.RESET_ALL)
      
      self.part_id       = None
      self.part_location = None
      
      # Getting the part data from database
      #dbaccess = DBaccess(database=f'trker_{self.database}', verbose=debug)
      db = self.db.database
      qy = f'select * from {db}.parts p where p.{Idt}=\'{id}\''
      rs = self.db.data_query(qy)
      try:
         data = rs[0]
         self.update_configuration('kind_of_part', data['kindOfPart'])
         
         self.part_id  = str(data['id'])
         self.part_location = self.locations[data['locationId']]['locationName']
         kopid = str(data['kindOfPartId'])
         
         qy = f'select * from {db}.p{kopid} p where p.{Idt}=\'{id}\''
         try:
            self.data = self.db.data_query(qy)[0]
         except Exception as e:
            print(e)
            exit(1)
      except:
         if self.mode.name!='expert':
            print( 'Component '+Fore.BLUE+f'{id}'+ Style.RESET_ALL+f' not registered in database {db}!' )
            print( 'Component registration is allowed only in expert mode.' )
            sys.exit(1)
         
         self.data = {}
         self.mandatory += ['unique_location']
         
         # get the kind_of_part from user input if not in default
         if 'kind_of_part' not in self.configuration.keys():  
            self.update_configuration('kind_of_part',self.get_kop(id))
         
         # get the location from user input if not in defaul
         if 'unique_location' not in self.configuration.keys():
            self.update_configuration('unique_location',self.get_location())
         self.part_location = self.retrieve('unique_location')
         
         # Add customization by kind_of_part
         kop = self.retrieve('kind_of_part')
         if kop=='2S Front-end Hybrid' or\
            kop=='2S Service Hybrid'   or\
            kop=='PS Front-end Hybrid' or\
            kop=='PS Read-out Hybrid'  or\
            kop=='PS Power Hybrid':
            from Hybrid import HybridComponentT
            hybrid = HybridComponentT(id,self.part_location,inserter=inserter)
            if kop != hybrid.h_kind:
               raise NotRecognized(self.__class__.__name__,id,\
                     f'as serial number for a {kop}!')
            self.mandatory = hybrid.mandatory
            self.configuration.update(hybrid.configuration)
            self.configuration['serialNumber']=id
            self.configuration['barcode']=id
         elif kop=='CF 3-ply Panel' or\
              kop=='PS Baseplate':
            from Baseplate import BaseplateComponentT
            plate = BaseplateComponentT(id,self.part_location,self.retrieve('manufacturer'),\
                                     comment=self.retrieve('description'),inserter=inserter)
            self.mandatory = plate.mandatory
            self.configuration.update(plate.configuration)
         else:
            # General customization 
            self.configuration[self.format_dkey(Idt,'_')] = id
         
         
      
      kop = self.retrieve('kind_of_part')
      qy = f'select * from {db}.trkr_attributes_v a where a.kind_of_part=\'{kop}\''
      self.attributes = self.db.data_query(qy)
      
      

   def get_kop(self, id):
      """Gather the kind_of_part type from the user input."""
      msg  = 'Component '+Fore.BLUE+f'{id}'+ Style.RESET_ALL+f' not registered in database {self.db.database}!\n'
      msg += 'Choose the kind of part:'
      
      qy   = f'select * from {self.db.database}.kinds_of_part k order by k.NAME ASC'
      kops = {}
      for k in self.db.data_query(qy):
         if 'Prototype' not in k['name'] and 'Test' not in k['name'] and 'Universal' not in k['name']:
            kops[str(k['id'])] = k['name']

      kinds_of_parts = list(kops.values())
      return kinds_of_parts[self.get_choice(msg,kinds_of_parts,ntab=2)]
   
   
   def get_location(self):
      """Gather the location from the user input."""
      msg  = 'Choose the location for the component '+Fore.BLUE+f'{self.name}'
      msg += Style.RESET_ALL+':'
      
      locations = [l['locationName'] for l in list(self.locations.values())]
      
      return locations[self.get_choice(msg,locations,ntab=3)]
      
   
   def add_children(self,child):
      """Add a child."""
      self.children.append(child)
      
      
   def steer_update(self):
      """Main steering routine for updating the component."""
      #Print command options
      cst = Back.BLACK+Fore.WHITE+Style.BOLD
      
      #Version, Product Date and Batch Number are not updated by DB-Loader if part is existing
      nac = Style.NORMAL if self.part_id==None else Fore.CYAN
      
      cmd1 = cst+'M'+Style.NORMAL+'-manufacturer '+Style.RESET_ALL+\
             '  '+cst+nac+'P'+    '-product date  '+Style.RESET_ALL+\
             '  '+cst+nac+'V'+    '-version '+Style.RESET_ALL+\
             '  '+cst+nac+'X'+    '-batch number '+Style.RESET_ALL+\
             '  '+cst+'D'+Style.NORMAL+'-description '+Style.RESET_ALL
      cmd2 = cst+'A'+Style.NORMAL+'-attributes   '+Style.RESET_ALL+\
             '  '+cst+'Q'+Style.NORMAL+'-quit/next     '+Style.RESET_ALL
      cmd3 = cst+'A'+Style.NORMAL+'-attributes   '+Style.RESET_ALL+\
             '  '+cst+'S'+Style.NORMAL+'-serial number '+Style.RESET_ALL+\
             '  '+cst+'B'+Style.NORMAL+'-barcode '+Style.RESET_ALL+\
             '  '+cst+'N'+Style.NORMAL+'-name label   '+Style.RESET_ALL+\
             '  '+cst+'Q'+Style.NORMAL+'-quit/next   '+Style.RESET_ALL

         
      while True:  # making a loop
         clear()
         print(self.stream_data())
         print(cmd1)

         if self.mode.name=='basic': print(cmd2)
         else: print(cmd3)
         choice = input(f'\nYour choice: ').lower()
         if choice == 's' and self.mode.name=='expert':
            self.update_data('serial number','serialNumber')
         if choice == 'b' and self.mode.name=='expert':
            self.update_data('barcode','barcode')
         if choice == 'n' and self.mode.name=='expert':
            self.update_data('name label','nameLabel')
         if choice == 'm':
            self.update_manufacturer()
         if choice == 'v' and self.part_id==None:
            self.update_data('version','version')
         if choice == 'p' and self.part_id==None:
            self.update_data('product date','product_date')
         if choice == 'x' and self.part_id==None:
            self.update_data('batch number','batch_number')
         if choice == 'd':
            self.update_data('description','description')
         if choice == 'a':
            self.update_attribute()
            
         if choice == 'q':
            break  # finishing the loop
      
   def update_data(self,name,conf):
      """Update the component descriptive data."""
      value = input(f'Enter new {name}: ')
      if name=='product date':
         import datetime
         try:
            datetime.datetime.strptime(value, '%Y-%m-%d')
         except ValueError:
            print( Fore.BLUE+f'Incorrect data format ({value}) , should be YYYY-MM-DD')
            print(Fore.BLUE+'not updating ....')
            time.sleep(2)
            return
     
      old,updated = self.get_data(conf)
      if value==old:
         if updated!='': self.configuration.pop(conf)
      else:
         self.configuration[conf]=value


   def update_manufacturer(self):
      """Update the manufacturer values."""
      choices = list(self.manufacturers.values())
      choice  = self.get_choice('Choose the manufacturer:',choices,True)
      if choice!='':
         man = choices[choice]
      
         old_man,upd_man = self.get_data('manufacturer')
      
         if man==old_man:
            if upd_man!='': self.configuration.pop('manufacturer')
         else: 
            self.configuration['manufacturer']=man
      

            
   def update_attribute(self):
      """Update the attribute values."""
      kop      = self.retrieve('kind_of_part')
      isExpert = self.mode.name=='expert'
      
      exa = [] if kop not in self.expert_att.keys() else self.expert_att[kop]
      
      choices = [a['attributeName'] for a in self.attributes if isExpert or \
                 a['attributeName'] not in exa ]
      choice = self.get_choice('Choose the attribute to update:',choices,True)
      attribute = '' if choice=='' else choices[choice]
      
      if attribute!='':
         old_value,new_value=self.get_att_data(attribute)
         current = old_value 
         if new_value!='': current = '<null>' if new_value==old_value else new_value
         values = [ v for v in self.get_att_values(attribute) if v!=old_value ]
         chvals = values.copy()
         if (old_value!='<null>'):
            values.insert(0,old_value)
            chvals.insert(0,f'Remove {attribute}')
         msg = f'Current setting for '+Fore.RED+f'{attribute}'+Style.RESET_ALL+f' is {current}; set to ...'
         choice = self.get_choice(msg,chvals,True)
         value = '' if choice=='' else values[choice]
         if value!='': self.set_attribute(attribute,old_value,value)
            

   def set_attribute(self,name,current,value):
      """Set the new attribute value."""
      attributes = self.retrieve('attributes')
      try:
         new_attributes = [a for a in attributes if a[0]!=name]
      except:
         new_attributes = []
         
      if current==value:  new_attributes.append((name,value,'delete'))
      else:               new_attributes.append((name,value))
      
      if len(new_attributes)!=0: 
         self.configuration['attributes']=new_attributes
      else:                      
         self.configuration.pop('attributes',None)
      

   def get_att_values(self,name):
      """Return the allowd values for an attribute."""
      for a in self.attributes:
         if a['attributeName']==name:  return a['allowedValues'].split(',')
         
   
   def get_data(self,name):
      """Return both the current value and the updated value for a given parameter
         name."""
      value = '<null>' if name not in self.data.keys() else self.data[name]
      new_value = self.retrieve(name) if self.retrieve(name)!=None else ''
      return value,new_value
   
   
   def get_att_data(self,name):
      """Return both the current attribute value and the updated attribute 
         for a given attribute name."""
      aname = self.format_dkey(name,' ','a')
      value = '<null>' if aname not in self.data.keys() else self.data[aname]
      aupd = self.retrieve('attributes')
      new_value = ''
      if aupd!=None:
         for au in aupd:
            if au[0]==name:   new_value = au[1]
      return value,new_value


   def format_dkey(self,name,chsp,prepend=''):
      """Format the attribute name to match the query output"""
      words = [w.title() for w in name.split(chsp)]
      words[0] = words[0].lower()
      return prepend+''.join(words)


   def get_choice(self,msg,choices,return_with_no_choice=False, ntab=None):
      """Get the choice from the user input."""
      while True:  # making a loop for manufacturer selection
         clear()
         print ( msg )
         self.stream_choices(choices,ntab)
         usel = input('\nYour choice: ')
         choice = ''
         try:
            choice = int(usel)
         except:
            if usel.lower()=='r' and return_with_no_choice:
               break
         if choice!='' and choice<len(choices):
            break
         
      return choice


   def stream_choices(self,choices,ntab=None):
      """Stream a list of choices for the user."""
      if ntab==None:
         ntab = int(len(choices)/20) + 1
         
      if ntab>4:  ntab = 4   # max 4 tab allowed
      if ntab<=0: ntab = 1   # min 1 tab allowed
       
      nsize = 3
      dsize = 32
      
      if ntab==4:  dsize=13
      if ntab==3:  dsize=19
      if ntab==2:  dsize=32
      if ntab==1:  dsize=72
      
      limit=[]
      start = 0
      step  = int(len(choices) /ntab)
      stepm = len(choices) % ntab
      for t in range(ntab):
         offs = 0 if stepm>0 else -1
         stop = start + step + offs
         limit.append((start,stop))
         start = stop + 1
         stepm -= 1
      #print(limit)
      choices_str = []
      for i,a in enumerate(choices):
         choices_str.append(Fore.BLACK+Style.BOLD+f' {i:>{nsize}} - '+Style.RESET_ALL+Fore.RED+f'{choices[i]:<{dsize}}'+Style.RESET_ALL)

      for i in range(int(len(choices)/ntab)):
         string = ''
         for t in range(ntab):
            position = limit[t][0]+i
            if position<=limit[t][1]:
               string += choices_str[position]
         print(string)
         #print(Fore.BLACK+Style.BOLD+f'{i:>3} - '+Style.RESET_ALL+Fore.RED+f'{choices[i]}'+Style.RESET_ALL)
      
      print(Fore.BLACK+Style.BOLD+f'  R - '+Style.RESET_ALL+f'return'+Style.RESET_ALL)
   
   
   def stream_data(self):
      """Stream how the component data will be updated."""
      
      k = self.data.keys()
      c = self.configuration.keys()
      
      parent_id    = '<null>' if 'partParentId' not in k else self.data['partParentId']
      serial       = '<null>' if 'serialNumber' not in k else self.data['serialNumber']
      barcode      = '<null>' if 'barcode' not in k else self.data['barcode']
      name_label   = '<null>' if 'nameLabel' not in k else self.data['nameLabel']
      version      = '<null>' if 'version' not in k else self.data['version']
      prod_date    = '<null>' if 'productionDate' not in k else self.data['productionDate']
      description  = '<null>' if 'description' not in k else self.data['description']
      manufacturer = '<null>' if 'manufacturer' not in k else self.data['manufacturer']
      batch_number = '<null>' if 'batchNumber' not in k else self.data['batchNumber']
      
      serial_up       = '' if 'serialNumber' not in c else self.retrieve('serialNumber')
      barcode_up      = '' if 'barcode' not in c else self.retrieve('barcode')
      name_label_up   = '' if 'nameLabel' not in c else self.retrieve('nameLabel')
      version_up      = '' if 'version' not in c else self.retrieve('version')
      prod_date_up    = '' if 'product_date' not in c else self.retrieve('product_date')
      description_up  = '' if 'description' not in c else self.retrieve('description')
      manufacturer_up = '' if 'manufacturer' not in c else self.retrieve('manufacturer')
      batch_number_up = '' if 'batch_number' not in c else self.retrieve('batch_number')
            
      txt = []
      c_item  = Fore.RED #Fore.LIGHTRED_EX if self.mode.name=='basic' else Fore.RED
      c_value = Fore.LIGHTBLACK_EX if self.mode.name=='basic' else Fore.BLACK
      nac = Style.NORMAL if self.part_id==None else Fore.LIGHTBLACK_EX
      
      kind_of_part = self.retrieve('kind_of_part')
      if self.part_id!=None:
         txt.append( f'Updating {kind_of_part} ' + Fore.BLUE + self.name + '\n')
      else:
         txt.append( f'Registering {kind_of_part} ' + Fore.BLUE + self.name + '\n')
      
      txt.append(c_item+'  serial number  '+c_value+f' : {serial}')
      txt.append(Fore.BLUE+f' -> {serial_up}\n' if serial_up!='' else '\n')
      txt.append(c_item+'  barcode        '+c_value+f' : {barcode}')
      txt.append(Fore.BLUE+f' -> {barcode_up}\n' if barcode_up!='' else '\n')
      txt.append(c_item+'  name label     '+c_value+f' : {name_label}')
      txt.append(Fore.BLUE+f' -> {name_label_up}\n' if name_label_up!='' else '\n')
      txt.append(c_item+'  location       '+Fore.LIGHTBLACK_EX+f' : {self.part_location}\n')
      #txt.append(Fore.BLUE+f' -> {name_label_up}\n' if name_label_up!='' else '\n')
      txt.append(Fore.RED+'  manufacturer   '+Fore.BLACK+f' : {manufacturer}')
      txt.append(Fore.BLUE+f' -> {manufacturer_up}\n' if manufacturer_up!='' else '\n')  
      txt.append(Fore.RED+'  version        '+Fore.BLACK+nac+f' : {version}')
      txt.append(Fore.BLUE+f' -> {version_up}\n' if version_up!='' else '\n')
      txt.append(Fore.RED+'  production date'+Fore.BLACK+nac+f' : {prod_date}')
      txt.append(Fore.BLUE+f' -> {prod_date_up}\n' if prod_date_up!='' else '\n')
      txt.append(Fore.RED+'  batch number   '+Fore.BLACK+nac+f' : {batch_number}')
      txt.append(Fore.BLUE+f' -> {batch_number_up}\n' if batch_number_up!='' else '\n')  
      txt.append(Fore.RED+'  description    '+Fore.BLACK+f' : {description}')
      txt.append(Fore.BLUE+f' -> {description_up}\n' if description_up!='' else '\n')  
      txt.append(Fore.MAGENTA+'--- attributes -----------'+Fore.BLACK+'\n')
      for a in self.attributes:
         name  = a['attributeName']
         value,new_value = self.get_att_data(name)
         current=''
         if new_value!='': current = '<null>' if new_value==value else new_value
         
         kop = self.retrieve('kind_of_part')
         exa = [] if kop not in self.expert_att.keys() else self.expert_att[kop]
         
         if name in exa: txt.append(c_item+f'  {name:<30}'+c_value+f' : {value}')
         else:        txt.append(c_item+f'  {name:<30}'+Fore.BLACK+f' : {value}')
         
         txt.append(Fore.BLUE+f' -> {current}\n' if current!='' else '\n'+Style.RESET_ALL)  

      return ''.join(txt)


   def xml_builder(self,parts):
      """Process data."""
      if len(self.configuration.keys()) <=1 and len(self.children)==0:  return
      # Gather the IDs of the component
      serial_number = self.retrieve('serialNumber')
      barcode       = self.retrieve('barcode')
      name_label    = self.retrieve('nameLabel')
      extended_data = self.retrieve('extended_data')
      
      if self.verbose==True:  print(self)
      
      if self.part_id!=None:
         self.update_parts_on_xml(parts, self.part_id, serial_number, barcode, name_label, extended_data)
      else:
         self.build_parts_on_xml(parts, serial_number, barcode, name_label, extended_data)
      
      if len(self.children)!=0:
         children = etree.SubElement(parts,"CHILDREN")
         for child in self.children:
            child.xml_builder(children)



   def dump_xml_data(self, filename='', tag='_update'):
      """Writes the XML file for the database upload. It requires in input
         the name of the XML file to be produced."""
      root      = etree.Element("ROOT")
      if self.inserter is not None:
         root.set('operator_name',self.inserter)

      parts = etree.SubElement(root,"PARTS")
      self.xml_builder(parts)

      filename = f'{self.name}{tag}.xml'
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      return filename



class Upload(BaseUploader):
   """Class to produce the xml file for uploading a generic part in the database.
   """

   def __init__(self, name, cdict, Ids, Idt='serial'):
      """Constructor: it requires the class name (name), the  dict of configuration
         (cdict) defining the ancillary data needed for the database upload, the
         list of Id (Ids) of the component to update and the type of id (idt) .
      """

      BaseUploader.__init__(self, name, configuration=cdict)

      self.Ids = Ids
      self.Idt = IdType[Idt]

      self.mandatory += ['kind_of_part','locations','unique_location',\
                         'manufacturer','attributes']


   def dump_xml_data(self, filename=''):
      """Writes the XML file for the database upload. It requires in input
         the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      parts = etree.SubElement(root,"PARTS")

      try:
         from rhapi import RhApi
      except:
         print( 'Cannot import rhapi: resthub package not in the path.' )
         print( 'Download the resthub package and run bin/setup.sh' )
         sys.exit(1)

      dburl = 'https://cmsdca.cern.ch/trk_rhapi'
      DBapi = RhApi(dburl, False, sso='login')

      serial_number = None
      barcode       = None
      name_label    = None

      for ID in self.Ids:
         qy = ''
         if self.Idt.name=='serial':
            serial_number = ID
            qy='select p.id from trker_cmsr.parts p where p.serial_number=\'%s\''%ID
         elif self.Idt.name=='barcode':
            qy='select p.id from trker_cmsr.parts p where p.barcode=\'%s\''%ID
            barcode = ID
         elif self.Idt.name=='name_label':
            qy='select p.id from trker_cmsr.parts p where p.name_label=\'%s\''%ID
            name_label = ID

         DBapi.clean(DBapi.qid(qy) )
         Qres  = DBapi.json2(qy)
         try:
            part_id = str( Qres['data'][0]['id'] )
            if self.verbose:
               print( 'skipping {} becasuse it already exist in DB.'.format(ID))
            continue
         except:
            if self.verbose:
               print( 'writing {} in XML file.'.format(ID))

         self.build_parts_on_xml(parts, serial_number, barcode, name_label)


      if filename == '':
        filename = '%s_upload.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
