from  Utils import DBaccess

class Query(DBaccess):
    def __init__(self, database='trker_cmsr', login_type='login', verbose=False):
        super(Query, self).__init__(database, login_type, verbose)
   
    def get_locations(self):
        query = "SELECT l.* FROM {}.trkr_locations_v l".format(self.database)

        print(query)
        data = self.data_query(query)
        return data

    def get_condition_data_tables(self, pTableName = None):
        if pTableName is None:
            query = "SELECT c.* FROM {}.conditions c\
                    ORDER BY c.id".format(self.database)
        else:
            query = "SELECT c.* FROM {}.conditions c WHERE c.database_table='{}'".format(self.database,pTableName)

        print(query)
        data = self.data_query(query)  
        return data

    def get_part_data_tables(self, pPartName = None):
        if pPartName is None:
            query = "SELECT k.* FROM {}.kinds_of_part k".format(self.database)
        else:
            query = "SELECT k.* FROM {}.kinds_of_part k WHERE k.name='{}'".format(self.database,pPartName)

        print(query)
        data = self.data_query(query)
        return data

    def get_ot_sensor_metrology(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_SENSOR_MTRLGY')[0]["conditionTable"]

        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    ORDER BY c.condition_data_set_id DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table,pModule)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)
        
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data


    def get_ot_module_metrology(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_MODULE_MTRLGY')[0]["conditionTable"]

        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    ORDER BY c.condition_data_set_id DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table,pModule)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)
        
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data
    
    def get_ot_module_iv(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_MODULE_IV')[0]["conditionTable"]

        if pGetLast:
            #Get highgest condidition data set id
            max_id_query = "SELECT c.condition_data_set_id \
                        FROM {}.{} c \
                        WHERE c.part_name_label='{}' \
                        ORDER BY c.condition_data_set_id DESC \
                        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table, pModule)

            print(max_id_query.replace("  ",""))
            max_id = self.data_query(max_id_query)[0]["conditionDataSetId"]

            #get all entries with highest condition data set id
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    AND c.condition_data_set_id={} \
                    ORDER BY c.condition_data_set_id DESC".format(self.database,table, pModule,max_id)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_sensor_iv(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_SENSOR_IV')[0]["conditionTable"]

        if pGetLast:
            #Get highgest condidition data set id
            max_id_query = "SELECT c.condition_data_set_id \
                        FROM {}.{} c \
                        WHERE c.part_name_label='{}' \
                        ORDER BY c.condition_data_set_id DESC \
                        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table, pModule)

            print(max_id_query.replace("  ",""))
            max_id = self.data_query(max_id_query)[0]["conditionDataSetId"]

            #get all entries with highest condition data set id
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    AND c.condition_data_set_id={} \
                    ORDER BY c.condition_data_set_id DESC".format(self.database,table, pModule,max_id)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        return data


    def get_ot_module_root_file_list( self, pModule, pGetLast = False):
        table = self.get_condition_data_tables('TEST_ROOT_FILES')[0]["conditionTable"]

        if pGetLast:
            #Get highgest condidition data set id
            max_id_query = "SELECT c.condition_data_set_id \
                        FROM {}.{} c \
                        WHERE c.part_name_label='{}' \
                        ORDER BY c.condition_data_set_id DESC \
                        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table, pModule)

            print(max_id_query.replace("  ",""))
            max_id = self.data_query(max_id_query)[0]["conditionDataSetId"]

            #get all entries with highest condition data set id
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    AND c.condition_data_set_id={} \
                    ORDER BY c.condition_data_set_id DESC".format(self.database,table, pModule,max_id)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        return data



    def get_ot_test_conditions_2s(self, pGetLast = False):
        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_2s_test_conditions_v c \
                    ORDER BY c.version DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database)
        else:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_2s_test_conditions_v c \
                    ORDER BY c.version DESC".format(self.database)
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_test_conditions_ps(self, pGetLast = False):
        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_ps_test_conditions_v c \
                    ORDER BY c.version DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database)
        else:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_ps_test_conditions_v c \
                    ORDER BY c.version DESC".format(self.database)
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_test_conditions_skeleton(self, pGetLast = False):
        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_skeleton_test_conditions_v c \
                    ORDER BY c.version DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database)
        else:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_skeleton_test_conditions_v c \
                    ORDER BY c.version DESC".format(self.database)
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_module_information_2s(self, pModule):     
        info = {"fehLSerial": "",
                "fehRSerial": "",
                "topSensorNameLabel": "",
                "bottomSensorNameLabel": ""}

        tables = self.get_part_data_tables()

        module_table    = next(table for table in tables if table["name"] == '2S Module'            )['partTable']
        feh_table       = next(table for table in tables if table["name"] == '2S Front-end Hybrid'  )['partTable']
        seh_table       = next(table for table in tables if table["name"] == '2S Service Hybrid'    )['partTable']
        sensor_table    = next(table for table in tables if table["name"] == '2S Sensor'            )['partTable']
        
        query = "SELECT pp.name_label AS module_name, pp.location, pp.description,\
                pp.a2s_sensor_spacing AS spacing, pp.astatus AS module_status, pp.aposition_index AS position_index,\
                pp.aring_index AS ring_index, pp.amodule_integration_status AS module_integration_status, pp.acontact_points AS contact_points,\
                part.manufacturer, \
                s.a2s_sensor_posn AS sensor_position, s.name_label AS sensor_name_label,\
                feh.afe_hybrid_side AS feh_hybrid_side, feh.serial_number AS feh_serial, \
                seh.serial_number AS seh_serial \
                FROM {db}.parts part \
                INNER JOIN {db}.{mod_t} pp ON pp.ID = part.ID \
                LEFT JOIN {db}.trkr_relationships_v r ON r.parent_name_label=pp.name_label \
                LEFT JOIN {db}.{feh_t} feh ON r.child_serial_number=feh.serial_number \
                LEFT JOIN {db}.{seh_t} seh ON r.child_serial_number=seh.serial_number \
                LEFT JOIN {db}.{s_t} s ON r.child_name_label=s.name_label \
                WHERE pp.name_label='{module}'".format( db      = self.database,
                                                        mod_t   = module_table,
                                                        feh_t   = feh_table,
                                                        seh_t   = seh_table,
                                                        s_t     = sensor_table,
                                                        module  = pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        print(data)
        for dataPoint in data:
            info.update(dataPoint)
            info["fehLSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","") == "Left"  else info["fehLSerial"] 
            info["fehRSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","") == "Right" else info["fehRSerial"] 
            info["topSensorNameLabel"]    = dataPoint["sensorNameLabel"] if dataPoint.get("sensorPosition","") == "TOP"    else info["topSensorNameLabel"] 
            info["bottomSensorNameLabel"] = dataPoint["sensorNameLabel"] if dataPoint.get("sensorPosition","") == "BOTTOM" else info["bottomSensorNameLabel"] 

        info.pop("fehHybridSide", None)
        info.pop("sensorPosition", None)
        info.pop("fehSerial", None)
        info.pop("sensorNameLabel", None)
        if len(data)==0:
            return {}
        else:
            return [info]
        
    def get_ot_module_information_ps(self, pModule):
        info = {"fehLSerial": "",
                "fehRSerial": ""}

        tables = self.get_part_data_tables()

        module_table = next(table for table in tables if table["name"] == 'PS Module'            )['partTable']
        feh_table    = next(table for table in tables if table["name"] == 'PS Front-end Hybrid'  )['partTable']
        poh_table    = next(table for table in tables if table["name"] == 'PS Service Hybrid'    )['partTable']
        roh_table    = next(table for table in tables if table["name"] == 'PS Read-out Hybrid'   )['partTable']
        pss_table    = next(table for table in tables if table["name"] == 'PS-p Sensor'          )['partTable']
        psp_table    = next(table for table in tables if table["name"] == 'PS-s Sensor'          )['partTable']

        query = "select pp.name_label AS module_name, pp.location, pp.description, \
                part.manufacturer, \
                pp.aps_sensor_spacing AS spacing, pp.astatus AS module_status, pp.aposition_index AS position_index,\
                pp.aring_index AS ring_index, pp.amodule_integration_status AS module_integration_status, \
                pss.name_label AS pss_name_label, \
                psp.name_label AS psp_name_label, \
                feh.afe_hybrid_side AS feh_hybrid_side, feh.serial_number AS feh_serial, \
                roh.serial_number AS roh_serial, roh.alpgbt_bandwidth AS lpgbt_bandwidth, \
                poh.serial_number AS poh_serial \
                FROM {db}.parts part \
                INNER JOIN {db}.{mod_t} pp ON pp.ID = part.ID \
                LEFT JOIN {db}.trkr_relationships_v r ON r.parent_name_label=pp.name_label \
                LEFT JOIN {db}.{feh_t} feh ON r.child_serial_number=feh.serial_number \
                LEFT JOIN {db}.{roh_t} roh ON r.child_serial_number=roh.serial_number \
                LEFT JOIN {db}.{poh_t} poh ON r.child_serial_number=poh.serial_number \
                LEFT JOIN {db}.{pss_t} pss ON r.child_name_label=pss.name_label \
                LEFT JOIN {db}.{psp_t} psp ON r.child_name_label=psp.name_label \
                WHERE pp.name_label='{module}'".format( db      = self.database,
                                                        mod_t   = module_table,
                                                        feh_t   = feh_table,
                                                        roh_t   = roh_table,
                                                        poh_t   = poh_table,
                                                        pss_t   = pss_table,
                                                        psp_t   = psp_table,
                                                        module  = pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)

        for dataPoint in data:
            info.update(dataPoint)
            info["fehLSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","")=="Left" else info["fehLSerial"] 
            info["fehRSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","")=="Right" else info["fehRSerial"]

        info.pop("fehHybridSide", None)
        info.pop("fehSerial", None)

        if len(data)==0:
            return {}
        else:
            return [info]

    def get_ot_boxes( self ):
        query = "SELECT p.name_label, l.location_name\
                 FROM {db}.parts p \
                INNER JOIN {db}.trkr_locations_v l ON l.location_id=p.location_id \
                WHERE p.kind_of_part_id=14600".format(db=self.database)
        print(query)
        data = self.data_query(query)
        locations = {}
        for dataPoint in data:
            locations[dataPoint["locationName"]]=0

        for dataPoint in data:
            locations[dataPoint["locationName"]] +=1

        [print(location,"\t",number) for location, number in locations.items()]

        return data

    def test( self ):
        to_test = {
         "rootFiles": 18600,
         "sensorMetrology":9420,
         "sensorIV": 1700,       
         "moduleIV":9500,
         "moduleMetrology":9460,
         "wireBondPullTest":9440,
         "missingBonds":9480}

        for key, value in to_test.items():

            query = "SELECT d.location \
                    FROM {db}.datasets d \
                    INNER JOIN {db}.parts p ON p.ID=d.part_id\
                    WHERE d.kind_of_condition_id={v}\
                    AND (p.kind_of_part_id IN (9000,9020)\
                    OR (p.kind_of_part_id IN (1120) AND p.part_parent_id NOT IN (1000)))".format(db=self.database,v=value)

            content = []
            #print(query.replace("  ",""))
            data = self.data_query(query)
            #print(data)
            [content.append(dataPoint.get("location","")) for dataPoint in data]
            print(key,sorted(list(set(content))))

        return []