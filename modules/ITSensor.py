#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 20-Apr-2024
# This module contains the classes that produce the XML file for uploading
# the sensor, the sensor wafers and the sensor QC data.


from AnsiColor    import Fore, Style
from Exceptions   import MissingData, BadStripInconsistency, BadMeasurement,\
                         NotRecognized
from BaseUploader import BaseUploader,ComponentType, ConditionData
from DataReader   import scale2, consistency_check
from lxml         import etree
from time         import gmtime, strftime
from enum         import Enum
from datetime     import datetime
from copy         import deepcopy 

import os,sys

ITDataType    = Enum('ITDataType','iv cv summary')
ITSensType    = Enum('ITSensType','IT_Quad IT_Double IT_Single_3D')
ITHCutType    = Enum('ITHCutType','IT_CROC_P1 IT_CROC_P2 IT_RD53A_P2 IT_RD53A_P5')
ITHRegions    = Enum('ITHRegions','NW NE EN EE ES SE SW WS WW WN')
#ITHMDataType  = Enum('ITHMDataType','IV CV DIODECV IGV TC MM MMP METADATA')

class ITwafer(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the IT Wafer
      component data read from the txt file shipped by Hamamatsu."""

   def __init__(self, configuration, dreader, envelope_ids=None):
      """Constructor: it requires the dict for configuration (configuration)
         defining the user data for the upload and the text file data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where the component is located;
      """

      self.type = ComponentType.ITwafer
      cdict = deepcopy(configuration)

      # Use Hamamatsu as the default for Manufacturer and Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'Hamamatsu'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'Hamamatsu'

      # The class name is the name_label of the Wafer taken from the ID in the txt file.
      # The first three fields are used for the name,
      # should we use instead the exact char position to avoid typo?
      name = '_'.join(dreader.getDataFromTag('#ID').split('_')[0:4])
      #name = dreader.getDataFromTag('#ID')[:13]

      # Set the sensor type which is related to the kind of part type
      self.sensor_type = name.split('_')[1]
      cdict['kind_of_part'] = self.kind_of_part()
      cdict['batch_number'] = self.batch_number(name)

      # Set the production date and the extended data
      objDate = datetime.strptime(dreader.getDataFromTag('#Date'),'%d-%b-%y')
      cdict['product_date'] = datetime.strftime(objDate,'%Y-%m-%d')
      cdict['ingot_number'] = dreader.getDataFromTag('#Ingot No.')

      BaseUploader.__init__(self, name, cdict, dreader)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                          'attributes']

      # initialize the envelope_ids: for the envelope ID correction is must
      # be assesed that the fields are coorect or that the position in the
      # string is always the same. Commented out for the time being
      self.envelope_ids = set()
      #if envelope_ids!=None:
      #   data = envelope_ids.getDataAsCWiseVector()
      #   self.envelope_ids = sorted( { n[:13] for n in data[0] if n!='' } )


   def barcode(self):
      """Returns the barcode as the name of the component."""
      return self.name

   def batch_number(self,barcode=None):
      """Return the batch number from the barcode: extract the field nr. 2"""
      if barcode==None:  barcode = self.name
      #print('batch number ',barcode.split('_')[2])
      return barcode.split('_')[2]

   def wafer_number(self,barcode=None):
      """Return the wafer number from the barcode: extract the field nr. 3"""
      if barcode==None:  barcode = self.name
      #print('wafer number ',barcode.split('_')[3])
      return barcode.split('_')[3]

   def wafer_type(self,barcode=None):
      """Return the wafer type from the barcode extract field nr. 1"""
      if barcode==None:  barcode = self.name
      #print('wafer type ',barcode.split('_')[1])
      return barcode.split('_')[1]

   def kind_of_part(self):
      """Return the kind_of_part to be associated to this sensor type."""
      # Singl structures IT_Single_P2, IT_Single_P1, IT_RD53A_P2, IT_RD53A_P5
      # to be implemented
      typ = self.sensor_type
      if   typ=='quad'   :   return "IT Quad Wafer"
      elif typ=='double' :   return "IT Double Wafer"
      elif typ=='single' :   return "IT 3D Wafer"
      else : raise NotRecognized(self.__class__.__name__,typ,' as an IT Sensor Type')

   def match(self,barcode):
      """Return True if the input barcode matches the class name."""
      if self.batch_number()==self.batch_number(barcode) and\
         self.wafer_number()==self.wafer_number(barcode) and\
         self.wafer_type()  ==self.wafer_type(barcode)  :
          return True
      return False

   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts)

      if filename == '':
         filename = '{}_wafer.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      batchN  = self.batch_number()
      waferN  = self.wafer_number()
      waferT  = self.wafer_type()
      label   = f'HPK_{waferT}_{batchN}_{waferN}'
      bcode   = f'HPK_{waferT}_{batchN}_{waferN}'

      for eid in self.envelope_ids:
          if self.match(eid):   bcode = eid

      # No extended data for the IT Wafers

      part_id = self.db.component_id(bcode,'barcode',self.kind_of_part())
      if part_id==None:
         # not registered
         self.build_parts_on_xml(parts,barcode=bcode,name_label=label)


class ITsensor(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the Sensor
      component data read from the txt file shipped by Hamamatsu."""

   def __init__(self, configuration, dreader, envelope_ids=None):
      """Constructor: it requires the dict for configuration (configuration)
         defining the ancillary data needed for the database upload and the
         worksheer data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
      """

      self.type = ComponentType.ITsensor
      cdict = deepcopy(configuration)

      # Use Hamamatsu as the default for Manufacturer and Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'Hamamatsu'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'Hamamatsu'

      # The class name is the name_label of the Wafer written in the txt file
      wpre = '_'.join(dreader.getDataFromTag('#ID').split('_')[0:4])
      styp = '-'.join(dreader.getDataFromTag('#ID').split('_')[4:])
      name = f'{wpre}_{styp}'

      # Set kind_of_part and batch number according to the Wafer name label
      self.sensor_type = dreader.getDataFromTag('#Sensor Type')
      cdict['kind_of_part'] = self.kind_of_part()
      cdict['batch_number'] = self.batch_number(name)

      # Set the production date and the extended data
      objDate = datetime.strptime(dreader.getDataFromTag('#Date'),'%d-%b-%y')
      cdict['product_date'] = datetime.strftime(objDate,'%Y-%m-%d')
      
      cdict['ingot_number']    = dreader.getDataFromTag('#Ingot No.')
      cdict['early_breakdown'] = dreader.getDataFromTag('#Early_breakdown')
      cdict['visual']          = dreader.getDataFromTag('#Visual')

      BaseUploader.__init__(self, name, cdict, dreader)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                         'attributes']

      # initialize the envelope_ids
      self.envelope_ids = set()
      #if envelope_ids!=None:
      #   data = envelope_ids.getDataAsCWiseVector()
      #   self.envelope_ids = sorted( { n[:19] for n in data[0] if n!='' } )


   def barcode(self):
      """Returns the list_of_ids."""
      return self.name

   def batch_number(self,barcode=None):
      """Return the batch number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[2]

   def wafer_number(self,barcode=None):
      """Return the sensor number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[3]

   def wafer_type(self,barcode=None):
      """Return the wafer type from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[1]

   def part_identifier(self,barcode=None):
      """Return the part identifier from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[4]

   def match(self,barcode):
      """Return True if the inout barcode matches the class name."""
      if self.batch_number()    == self.batch_number(barcode) and\
         self.wafer_number()    == self.wafer_number(barcode) and\
         self.wafer_type()      == self.wafer_type(barcode)   and\
         self.part_identifier() == self.part_identifier(barcode):
          return True
      return False

   def kind_of_part(self):
      """Return the kind_of_part to be associated to this sensor type."""
      # Singl structures IT_Single_P2, IT_Single_P1, IT_RD53A_P2, IT_RD53A_P5
      # to be implemented
      typ = self.sensor_type
      if   typ=='IT_Quad'       :   return "IT Quad Sensor"
      elif typ=='IT_Double'     :   return "IT Double Sensor"
      elif typ=='IT_Single_3D'  :   return "IT 3D Sensor"
      else : raise NotRecognized(self.__class__.__name__,typ,' as an IT Sensor Type')

   def dump_xml_data(self,filename='',wafers=None):
      """Writes the sensor component in the XML file for the database upload.
         It requires the name of the XML file to be produced. A SensorWafer
         class can also be input: in this case a parent-child relationship
         with the Sensor components is set (the wafer components must have
         been already entered in the database).
      """
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts,wafers)

      if filename == '':
         filename = '%s_sensors.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      wafers = args[0]

      children = parts
      if wafers is not None:
         for wafer in wafers:
            if wafer.match(self.barcode()):
               wafer_kop  = wafer.kind_of_part()
               wafer_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
               etree.SubElement(wafer_part,"BARCODE").text = wafer.barcode()
               children = etree.SubElement(wafer_part,"CHILDREN")

      batchN  = self.batch_number()
      waferN  = self.wafer_number()
      waferT  = self.wafer_type()
      partID  = self.part_identifier()
      label   = f'HPK_{waferT}_{batchN}_{waferN}_{partID}'
      bcode   = f'HPK_{waferT}_{batchN}_{waferN}_{partID}'
      sid        = self.reader.getDataFromTag('#Scratch PAD')
      
      #for eid in self.envelope_ids:
      #    if self.match(eid):   bcode = eid
      
      # extended data
      
      ex= [ ( 'INGOT_NR', self.retrieve('ingot_number') ) ]
      if self.retrieve('early_breakdown'):
         ex.append( ( 'EARLY_BREAKDOWN', self.retrieve('early_breakdown')) )
      if self.retrieve('visual'):
         ex.append( ( 'VISUAL', self.retrieve('visual')) )
      

      self.build_parts_on_xml(children,serial=sid,barcode=bcode,name_label=label,\
                               extended_data=ex)


class IThalfmoon(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the Halfmoon
      component data read from the txt file shipped by Hamamatsu."""

   def __init__(self, configuration, dreader, wafer=None, envelope_ids=None):
      """Constructor: it requires the dict for configuration (configuration)
         defining the ancillary data needed for the database upload and the
         worksheet data (dreader) or the wafer data in alternative to the 
         dreader .
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
      """

      self.type = ComponentType.IThalfmoon
      cdict = deepcopy(configuration)
      
      name = ''
      
      if wafer != None:
         #configure the halfmoon without the hamamatsu file
         cdict['manufacturer'] = wafer.retrieve('manufacturer')
         cdict['product_date'] = wafer.retrieve('product_date')
         cdict['ingot_number'] = wafer.retrieve('ingot_number')
         cdict['unique_location'] = wafer.retrieve('unique_location')
         name=cdict['name']
      else:
         # Use Hamamatsu as the default for Manufacturer and Location
         if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'Hamamatsu'
         if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'Hamamatsu'

         # Check if sensor type is allowed for the Halfmoon
         sensor_type = dreader.getDataFromTag('#Sensor Type')
         try:
            ITHCutType[str(sensor_type)]  # check if sensor type is recognized
         except:
            types = [el.name for el in ITHCutType]
            print(Fore.RED+f'{sensor_type}'+Style.RESET_ALL+' is not a valid ID type!')
            print('valid ID type are: '+Fore.BLUE+f'{types}'+Style.RESET_ALL)
            exit(1)

         # The class name is the name_label of the Wafer written in the txt file
         wpre = '_'.join(dreader.getDataFromTag('#ID').split('_')[0:4])
         styp = '-'.join(dreader.getDataFromTag('#ID').split('_')[4:])
         
         # Correct the last part of name label if there is a mismatch with sensor_type
         if styp.split('-')[1] != sensor_type.split('_')[1] and \
            sensor_type.split('_')[1] != 'CROC':
               part_tags = styp.split('-')
               styp = '-'.join([part_tags[0],sensor_type.split('_')[1],part_tags[2]])
      
         name = f'{wpre}_{styp}'
         
         # Set the production date and the extended data
         objDate = datetime.strptime(dreader.getDataFromTag('#Date'),'%d-%b-%y')
         cdict['product_date'] = datetime.strftime(objDate,'%Y-%m-%d')
      
         cdict['ingot_number']    = dreader.getDataFromTag('#Ingot No.')
         #cdict['early_breakdown'] = dreader.getDataFromTag('#Early_breakdown')
         cdict['visual']          = dreader.getDataFromTag('#Visual')
      
         # Set the extra Attributes for the Halfmoon
         sensor_name = sensor_type.split('_')[1] if sensor_type.split('_')[1]!="CROC" else "Single"
         sensor_tag = sensor_type.split('_')[2]
         try:
            new_attributes = [a for a in cdict['attributes']]
         except:
            new_attributes = []
         
         new_attributes.append(('IT Halfmoon Structure',f'{sensor_name} {sensor_tag}'))
         cdict['attributes']=new_attributes


      # Set kind_of_part and batch number according to the Wafer name label
      cdict['kind_of_part'] = self.kind_of_part()
      cdict['batch_number'] = self.batch_number(name)

      BaseUploader.__init__(self, name, cdict, dreader)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                         'attributes']


      # initialize the envelope_ids
      self.envelope_ids = set()
      #if envelope_ids!=None:
      #   data = envelope_ids.getDataAsCWiseVector()
      #   self.envelope_ids = sorted( { n[:19] for n in data[0] if n!='' } )


   def barcode(self):
      """Returns the list_of_ids."""
      return self.name

   def batch_number(self,barcode=None):
      """Return the batch number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[2]

   def wafer_number(self,barcode=None):
      """Return the sensor number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[3]

   def wafer_type(self,barcode=None):
      """Return the wafer type from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[1]

   def part_identifier(self,barcode=None):
      """Return the part identifier from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode.split('_')[4]

   def match(self,barcode):
      """Return True if the inout barcode matches the class name."""
      if self.batch_number()    == self.batch_number(barcode) and\
         self.wafer_number()    == self.wafer_number(barcode) and\
         self.wafer_type()      == self.wafer_type(barcode)   and\
         self.part_identifier() == self.part_identifier(barcode):
          return True
      return False

   def kind_of_part(self):
      """Return the kind_of_part to be associated to this sensor type."""
      # For Halfmoon there is a unique kind of part
      return "IT Halfmoon Cut"

   def dump_xml_data(self,filename='',wafers=None):
      """Writes the sensor component in the XML file for the database upload.
         It requires the name of the XML file to be produced. A SensorWafer
         class can also be input: in this case a parent-child relationship
         with the Sensor components is set (the wafer components must have
         been already entered in the database).
      """
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts,wafers)

      if filename == '':
         filename = '%s_sensors.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      wafers = args[0]

      children = parts
      if wafers is not None:
         for wafer in wafers:
            if wafer.match(self.barcode()):
               wafer_kop  = wafer.kind_of_part()
               wafer_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
               etree.SubElement(wafer_part,"BARCODE").text = wafer.barcode()
               children = etree.SubElement(wafer_part,"CHILDREN")

      batchN  = self.batch_number()
      waferN  = self.wafer_number()
      waferT  = self.wafer_type()
      partID  = self.part_identifier()
      label   = f'HPK_{waferT}_{batchN}_{waferN}_{partID}'
      bcode   = f'HPK_{waferT}_{batchN}_{waferN}_{partID}'
      sid     = self.reader.getDataFromTag('#Scratch PAD') \
                if self.reader is not None else None
      
      #for eid in self.envelope_ids:
      #    if self.match(eid):   bcode = eid
      
      # extended data
      
      ex= [ ( 'INGOT_NR', self.retrieve('ingot_number') ) ]
      if self.retrieve('early_breakdown'):
         ex.append( ( 'EARLY_BREAKDOWN', self.retrieve('early_breakdown')) )
      if self.retrieve('visual'):
         ex.append( ( 'VISUAL', self.retrieve('visual')) )
      

      self.build_parts_on_xml(children,serial=sid,barcode=bcode,name_label=label,\
                               extended_data=ex)


class ITsensorMeasurements(ConditionData):
   """Produces the XML file for storing the sensor measurement data."""
   iv_data        = {'cond_name' : 'Tracker IT Sensor IV Test',
                     'table_name': 'TEST_SENSOR_IV',
                     'DBvar_vs_TxtHeader' : {'VOLTS'       : 'voltage [V]',
                                             'CURRNT_NAMP' : 'current [A]',
                                             'TEMP_DEGC'   : 'Not in txt file',
                                             'RH_PRCNT'    : 'Not in txt file'
                                            },
                     'mandatory' : ['VOLTS','CURRNT_NAMP']
                    }
   cv_data        = {'cond_name' : 'Tracker IT Sensor CV Test',
                     'table_name': 'TEST_SENSOR_CV',
                     'DBvar_vs_TxtHeader' : {'VOLTS'        : 'voltage [V]',
                                             'CAPCTNC_PFRD' : 'capacitance [F]',
                                             'TEMP_DEGC'    : 'Not in txt file',
                                             'RH_PRCNT'     : 'Not in txt file'
                                            },
                     'mandatory' : ['VOLTS','CAPCTNC_PFRD']
                    }
   summary_data   = {'cond_name' : 'Tracker IT Sensor Summary Data',
                     'table_name': 'TEST_SENSOR_SMMRY',
                     'DBvar_vs_TxtHeader' : {
                                 'AV_TEMP_DEGC'     : '#temperature [deg]',
                                 'AV_RH_PRCNT'      : '#humidity [%]',
                                 'FREQ_HZ'          : 'Not in txt file',
                                 'LCR_MODE'         : 'Not in txt file',
                                 'LCR_AMP_V'        : 'Not in txt file',
                                 'DEPLETION_VLTG_V' : '#Depletion voltage [V]',
                                 'CMPLNC_VLTG_IV_V' : 'Not in txt file',
                                 'CMPLNC_VLTG_CV_V' : 'Not in txt file'
                                            },
                     'mandatory' : ['AV_TEMP_DEGC','AV_RH_PRCNT']
                    }

   def __init__(self, run_conf, data_conf, data_type, dreader, sensor_data):
      """Constructor: it requires the description of the run data (run_conf),
         the description of the measurement data (data_conf), the type of data
         (data_type), the data table of the measurements (dreader) and the
         sensor data (sensor_data).

         Mandatory configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
            kind_of_part:    the type of component whose data belong to;
            serial:          identifier of the component whose data belong to;
            barcode:         identifier of the component whose data belong to;
            name_label:      identifier of the component whose data belong to;
            cond_name:       kind_of_condition name corresponding to these data;
            table_name:      condition data table where data have to be recorded;
            data_version:    data set version;
            DBV_vs_Theader:  describe how to associate data to the DB variables;
         run_name and run_type + run_number are exclusive. Serial, barcode and
         name_label are mutual exclusive.

         Optional parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            data_comment: description/comment on the dataset
            inserter:     person recording the run metadata (override the user)
      """

      # measurement whose value must be inverted for VQC run
      self.invert_value = ['VOLTS','CURRNT_NAMP']
      self.typ = data_type
      self.compliance_voltage = ''

      # The class name is the name_label of the Wafer written in the txt file
      sensor  = ITsensor({},sensor_data)
      batchN  = sensor.batch_number()
      waferN  = sensor.wafer_number()
      waferT  = sensor.wafer_type()
      partID  = sensor.part_identifier()
      name_label = f'HPK_{waferT}_{batchN}_{waferN}_{partID}'

      # Set kind_of_part and batch number according to the Wafer name label
      objDate = datetime.strptime(sensor_data.getDataFromTag('#Date'),'%d-%b-%y')
      run_metadata = run_conf.copy()
      run_metadata['run_begin']    = datetime.strftime(objDate,'%Y-%m-%d')
      run_metadata['kind_of_part'] = sensor.kind_of_part()
      run_metadata['name_label']   = name_label

      run_metadata['run_operator'] = sensor_data.getDataFromTag('#Operator')
      run_metadata['run_location'] = 'Hamamatsu'

      configuration = {}
      configuration.update(run_metadata)
      configuration.update(data_conf)
      if data_type==ITDataType.iv:
         name = 'IV-{}'.format(sensor.name)
         configuration.update(self.iv_data)
         ConditionData.__init__(self, name, configuration, dreader)
      if data_type==ITDataType.cv:
         name = 'CV-{}'.format(sensor.name)
         configuration.update(self.cv_data)
         ConditionData.__init__(self, name, configuration, dreader)
      if data_type==ITDataType.summary:
         name = 'SUMMARY-{}'.format(sensor.name)
         configuration.update(self.summary_data)
         ConditionData.__init__(self, name, configuration, sensor_data)

   def build_data_block(self,dataset):
      """Steers the building of the data block."""

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      if self.typ==ITDataType.iv:
         if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
                data_description['VOLTS'],data_description['CURRNT_NAMP']):
            raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
         self.build_table_measurements(dataset)
      if self.typ==ITDataType.cv:
         if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
                data_description['VOLTS'],data_description['CAPCTNC_PFRD']):
            raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
         self.build_table_measurements(dataset)
      if self.typ==ITDataType.summary:
         self.build_summary_measurements(dataset)

   #def write_data_block(self):
   #   """Check if data block must be written."""
   #   if self.typ==ITDataType.bad_strip:
   #      bad_strips = 0
   #      try:
   #         bad_strips = int(self.reader.getDataFromTag('#Bad strips'))
   #      except:  pass
   #      if bad_strips==0: return False
   #   return True


   def build_table_measurements(self,dataset):
      """Build the data from the measurements table."""

      table_data = self.reader.getDataAsCWiseDictRowSplit()
      # scale to nano-ampere and pico-farad
      if self.typ == ITDataType.iv:   scale2(table_data,'current [A]',1e9)
      if self.typ == ITDataType.cv:   scale2(table_data,'capacitance [F]',1e12)

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for i,point in enumerate(table_data):
         data_elements = []
         skip_record   = False
         for var in data_description.keys():
            try:
               value = point[data_description[var]]
               x = float(value)
               if var in self.invert_value:    x = -x if x!=0. else x
               data_elements.append((var,str(x)))
            except  KeyError:
               # key not present in data file, if not mandatory pass
               mandatory = self.retrieve('mandatory')
               if var in mandatory:
                  raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
               pass
            except ValueError:
               skip_record = True

         if not skip_record:
            data = etree.SubElement(dataset,"DATA")
            for el in data_elements:
               etree.SubElement(data,"{}".format(el[0])).text = el[1]


   def build_summary_measurements(self,dataset):
      """build_table_measurementsld the data from the measurements table."""

      data = etree.SubElement(dataset,"DATA")
      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for var in data_description.keys():
         value = self.reader.getDataFromTag(data_description[var])
         if value != None:
            etree.SubElement(data,"{}".format(var)).text = value
      for child in self.children:
         compliance_voltage = child.get_compliance_voltage()
         if child.typ==ITDataType.iv and compliance_voltage!=None:
            etree.SubElement(data,'CMPLNC_VLTG_IV_V').text = compliance_voltage
         if child.typ==ITDataType.cv and compliance_voltage!=None:
            etree.SubElement(data,'CMPLNC_VLTG_CV_V').text = compliance_voltage


   def get_compliance_voltage(self):
      """Check the data to see if compliance voltage is reached."""
      table_data = self.reader.getDataAsCWiseDictRowSplit()
      # scale to nano-ampere and pico-farad
      if self.typ == ITDataType.iv:   scale2(table_data,'current [A]',1e9)
      if self.typ == ITDataType.cv:   scale2(table_data,'capacitance [F]',1e12)

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for i,point in enumerate(table_data):
         data_elements = []
         for var in data_description.keys():
            try:
               value = point[data_description[var]]
               x = float(value)
               if var in self.invert_value:    x = -x if x!=0. else x
               data_elements.append((var,str(x)))
            except  KeyError:
               # key not present in data file, if not mandatory pass
               mandatory = self.retrieve('mandatory')
               if var in mandatory:
                  raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
               pass
            except ValueError:
               if self.verbose:
                  print('compliance reached in {}'.format(self.reader.filename))
               #skip_record = True
               last_data = data_elements[-1]
               if last_data[0]=='VOLTS':
                  self.compliance_voltage=last_data[1]
                  return self.compliance_voltage
      return None


class IThalfmoonMeasurements(ConditionData):
   """Produces the XML file for storing the sensor measurement data."""
   iv_data        = {'cond_name' : 'Tracker IT Halfmoon IV Test',
                     'table_name': 'TEST_SENSOR_IV',
                     'DBvar_vs_TxtHeader' : {'VOLTS'       : 'voltage [V]',
                                             'CURRNT_NAMP' : 'current [A]',
                                             'TEMP_DEGC'   : 'Not in txt file',
                                             'RH_PRCNT'    : 'Not in txt file'
                                            },
                     'mandatory' : ['VOLTS','CURRNT_NAMP']
                    }
   cv_data        = {'cond_name' : 'Tracker IT Halfmoon CV Test',
                     'table_name': 'TEST_SENSOR_CV',
                     'DBvar_vs_TxtHeader' : {'VOLTS'        : 'voltage [V]',
                                             'CAPCTNC_PFRD' : 'capacitance [F]',
                                             'TEMP_DEGC'    : 'Not in txt file',
                                             'RH_PRCNT'     : 'Not in txt file'
                                            },
                     'mandatory' : ['VOLTS','CAPCTNC_PFRD']
                    }
   summary_data   = {'cond_name' : 'Tracker IT Halfmoon Summary Data',
                     'table_name': 'TEST_SENSOR_SMMRY',
                     'DBvar_vs_TxtHeader' : {
                                 'AV_TEMP_DEGC'     : '#temperature [deg]',
                                 'AV_RH_PRCNT'      : '#humidity [%]',
                                 'FREQ_HZ'          : 'Not in txt file',
                                 'LCR_MODE'         : 'Not in txt file',
                                 'LCR_AMP_V'        : 'Not in txt file',
                                 'DEPLETION_VLTG_V' : '#Depletion voltage [V]',
                                 'CMPLNC_VLTG_IV_V' : 'Not in txt file',
                                 'CMPLNC_VLTG_CV_V' : 'Not in txt file'
                                            },
                     'mandatory' : ['AV_TEMP_DEGC','AV_RH_PRCNT']
                    }

   def __init__(self, run_conf, data_conf, data_type, dreader, halfmoon_data):
      """Constructor: it requires the description of the run data (run_conf),
         the description of the measurement data (data_conf), the type of data
         (data_type), the data table of the measurements (dreader) and the
         halfmoon data (halfmoon_data).

         Mandatory configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
            kind_of_part:    the type of component whose data belong to;
            serial:          identifier of the component whose data belong to;
            barcode:         identifier of the component whose data belong to;
            name_label:      identifier of the component whose data belong to;
            cond_name:       kind_of_condition name corresponding to these data;
            table_name:      condition data table where data have to be recorded;
            data_version:    data set version;
            DBV_vs_Theader:  describe how to associate data to the DB variables;
         run_name and run_type + run_number are exclusive. Serial, barcode and
         name_label are mutual exclusive.

         Optional parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            data_comment: description/comment on the dataset
            inserter:     person recording the run metadata (override the user)
      """

      # measurement whose value must be inverted for VQC run
      self.invert_value = ['VOLTS','CURRNT_NAMP']
      self.typ = data_type
      self.compliance_voltage = ''

      # The class name is the name_label of the Wafer written in the txt file
      halfmoon  = IThalfmoon({},halfmoon_data)
      batchN  = halfmoon.batch_number()
      waferN  = halfmoon.wafer_number()
      waferT  = halfmoon.wafer_type()
      partID  = halfmoon.part_identifier()
      name_label = f'HPK_{waferT}_{batchN}_{waferN}_{partID}'

      # Set kind_of_part and batch number according to the Wafer name label
      objDate = datetime.strptime(halfmoon_data.getDataFromTag('#Date'),'%d-%b-%y')
      run_metadata = run_conf.copy()
      run_metadata['run_begin']    = datetime.strftime(objDate,'%Y-%m-%d')
      run_metadata['kind_of_part'] = halfmoon.kind_of_part()
      run_metadata['name_label']   = name_label

      run_metadata['run_operator'] = halfmoon_data.getDataFromTag('#Operator')
      run_metadata['run_location'] = 'Hamamatsu'

      configuration = {}
      configuration.update(run_metadata)
      configuration.update(data_conf)
      if data_type==ITDataType.iv:
         name = 'IV-{}'.format(halfmoon.name)
         configuration.update(self.iv_data)
         ConditionData.__init__(self, name, configuration, dreader)
      if data_type==ITDataType.cv:
         name = 'CV-{}'.format(halfmoon.name)
         configuration.update(self.cv_data)
         ConditionData.__init__(self, name, configuration, dreader)
      if data_type==ITDataType.summary:
         name = 'SUMMARY-{}'.format(halfmoon.name)
         configuration.update(self.summary_data)
         ConditionData.__init__(self, name, configuration, halfmoon_data)

   def build_data_block(self,dataset):
      """Steers the building of the data block."""

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      if self.typ==ITDataType.iv:
         if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
                data_description['VOLTS'],data_description['CURRNT_NAMP']):
            raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
         self.build_table_measurements(dataset)
      if self.typ==ITDataType.cv:
         if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
                data_description['VOLTS'],data_description['CAPCTNC_PFRD']):
            raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
         self.build_table_measurements(dataset)
      if self.typ==ITDataType.summary:
         self.build_summary_measurements(dataset)

   #def write_data_block(self):
   #   """Check if data block must be written."""
   #   if self.typ==ITDataType.bad_strip:
   #      bad_strips = 0
   #      try:
   #         bad_strips = int(self.reader.getDataFromTag('#Bad strips'))
   #      except:  pass
   #      if bad_strips==0: return False
   #   return True


   def build_table_measurements(self,dataset):
      """Build the data from the measurements table."""

      table_data = self.reader.getDataAsCWiseDictRowSplit()
      # scale to nano-ampere and pico-farad
      if self.typ == ITDataType.iv:   scale2(table_data,'current [A]',1e9)
      if self.typ == ITDataType.cv:   scale2(table_data,'capacitance [F]',1e12)

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for i,point in enumerate(table_data):
         data_elements = []
         skip_record   = False
         for var in data_description.keys():
            try:
               value = point[data_description[var]]
               x = float(value)
               if var in self.invert_value:    x = -x if x!=0. else x
               data_elements.append((var,str(x)))
            except  KeyError:
               # key not present in data file, if not mandatory pass
               mandatory = self.retrieve('mandatory')
               if var in mandatory:
                  raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
               pass
            except ValueError:
               skip_record = True

         if not skip_record:
            data = etree.SubElement(dataset,"DATA")
            for el in data_elements:
               etree.SubElement(data,"{}".format(el[0])).text = el[1]


   def build_summary_measurements(self,dataset):
      """build_table_measurementsld the data from the measurements table."""

      data = etree.SubElement(dataset,"DATA")
      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for var in data_description.keys():
         value = self.reader.getDataFromTag(data_description[var])
         if value != None:
            etree.SubElement(data,"{}".format(var)).text = value
      for child in self.children:
         compliance_voltage = child.get_compliance_voltage()
         if child.typ==ITDataType.iv and compliance_voltage!=None:
            etree.SubElement(data,'CMPLNC_VLTG_IV_V').text = compliance_voltage
         if child.typ==ITDataType.cv and compliance_voltage!=None:
            etree.SubElement(data,'CMPLNC_VLTG_CV_V').text = compliance_voltage


   def get_compliance_voltage(self):
      """Check the data to see if compliance voltage is reached."""
      table_data = self.reader.getDataAsCWiseDictRowSplit()
      # scale to nano-ampere and pico-farad
      if self.typ == ITDataType.iv:   scale2(table_data,'current [A]',1e9)
      if self.typ == ITDataType.cv:   scale2(table_data,'capacitance [F]',1e12)

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for i,point in enumerate(table_data):
         data_elements = []
         for var in data_description.keys():
            try:
               value = point[data_description[var]]
               x = float(value)
               if var in self.invert_value:    x = -x if x!=0. else x
               data_elements.append((var,str(x)))
            except  KeyError:
               # key not present in data file, if not mandatory pass
               mandatory = self.retrieve('mandatory')
               if var in mandatory:
                  raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
               pass
            except ValueError:
               if self.verbose:
                  print('compliance reached in {}'.format(self.reader.filename))
               #skip_record = True
               last_data = data_elements[-1]
               if last_data[0]=='VOLTS':
                  self.compliance_voltage=last_data[1]
                  return self.compliance_voltage
      return None