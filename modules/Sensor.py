#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 23-Mar-2019
# Modified by Alessandro Di Mattia <dimattia@cern.ch>, 04-Jun-2020
#  + implementation of the new naming convention for the serial numbers

# This module contains the classes that produce the XML file for uploading
# the sensor, the sensor wafers and the sensor QC data.


from Exceptions   import MissingData, BadStripInconsistency, BadMeasurement,\
                         NotRecognized
from BaseUploader import BaseUploader,ComponentType, ConditionData
from DataReader   import scale2, consistency_check
from lxml         import etree
from time         import gmtime, strftime
from enum         import Enum
from datetime     import datetime

import os,sys

OTDataType    = Enum('OTDataType','iv cv bad_strip summary')
HMDataType    = Enum('HMDataType','IV CV DIODECV IGV TC MM MMP METADATA')

class OTwafer(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the OT Wafer
      component data read from the txt file shipped by Hamamatsu."""

   def __init__(self, configuration, dreader, envelope_ids=None):
      """Constructor: it requires the dict for configuration (configuration)
         defining the user data for the upload and the text file data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where the component is located;
      """

      self.type = ComponentType.OTwafer
      #self.database = database
      cdict = configuration.copy()

      # Use Hamamatsu as the default for Manufacturer and Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'Hamamatsu'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'Hamamatsu'

      # The class name is the name_label of the Wafer written in the txt file
      name = dreader.getDataFromTag('#ID')[:13]

      # Set kind_of_part and batch number according to the Wafer name label
      cdict['kind_of_part'] = self.kind_of_part(name)
      cdict['batch_number'] = self.batch_number(name)

      # Set the production date and the extended data
      objDate = datetime.strptime(dreader.getDataFromTag('#Date'),'%d-%b-%y')
      cdict['product_date'] = datetime.strftime(objDate,'%Y-%m-%d')
      cdict['ingot_number'] = dreader.getDataFromTag('#Ingot No.')

      #Check that Polysilicon Bias resistance are float
      try:
         value = float( dreader.getDataFromTag('#Polysilicon Bias Resistance Upper [Mohm]') )
         cdict['psbiasres_up'] = str(value)
      except:  pass
      try:
         value = float( dreader.getDataFromTag('#Polysilicon Bias Resistance Lower [Mohm]') )
         cdict['psbiasres_lo'] = str(value)
      except:  pass
      try:
         value = float( dreader.getDataFromTag('#Polysilicon Bias Resistance Average [Mohm]') )
         cdict['psbiasres_av'] = str(value)
      except:  pass

      BaseUploader.__init__(self, name, cdict, dreader)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                          'attributes']

      #initialize the envelope_ids
      self.envelope_ids = set()
      if envelope_ids!=None:
         data = envelope_ids.getDataAsCWiseVector()
         self.envelope_ids = sorted( { n[:13] for n in data[0] if n!='' } )


   def barcode(self):
      """Returns the list_of_ids."""
      return self.name

   def batch_number(self,barcode=None):
      """Return the batch number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode[0:5]

   def wafer_number(self,barcode=None):
      """Return the sensor number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode[6:9]

   def wafer_type(self,barcode=None):
      """Return the wafer type from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode[10:13]

   def kind_of_part(self,barcode=None):
      """Return the kind_of_part to be associated to this barcode."""
      if barcode==None:  barcode = self.name
      typ = self.wafer_type(barcode)
      if   typ=='2-S':   return "2S Wafer"
      elif typ=='PSS':   return "PS-s Wafer"
      elif typ=='PSP':   return "PS-p Wafer"
      return None

   def match(self,barcode):
      """Return True if the inout barcode matches the class name."""
      if self.batch_number()==self.batch_number(barcode) and\
         self.wafer_number()==self.wafer_number(barcode) and\
         self.wafer_type()  ==self.wafer_type(barcode)  :
          return True
      return False

   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts)

      if filename == '':
         filename = '{}_wafer.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      batchN  = self.batch_number()
      waferN  = self.wafer_number()
      waferT  = self.wafer_type()
      label   = '{}_{}_{}'.format(batchN,waferN,waferT)
      bcode   = '{}_{}_{}'.format(batchN,waferN,waferT)

      for eid in self.envelope_ids:
          if self.match(eid):   bcode = eid

      ex= [ ( 'INGOT_NR', self.retrieve('ingot_number') ) ]
      if self.retrieve('psbiasres_up'):
         ex.append( ( 'PBIASRES_UPPER_MOHM', self.retrieve('psbiasres_up')) )
      if self.retrieve('psbiasres_lo'):
         ex.append( ( 'PBIASRES_LOWER_MOHM', self.retrieve('psbiasres_lo')) )
      if self.retrieve('psbiasres_av'):
         ex.append( ( 'PBIASRES_AVERAGE_MOHM', self.retrieve('psbiasres_av')) )


      part_id = self.db.component_id(bcode,'barcode',self.kind_of_part())
      if part_id==None:
         # not registered
         self.build_parts_on_xml(parts,barcode=bcode,name_label=label,extended_data=ex)


class OTsensor(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the Sensor
      component data read from the txt file shipped by Hamamatsu."""

   def __init__(self, configuration, dreader, envelope_ids=None):
      """Constructor: it requires the dict for configuration (configuration)
         defining the ancillary data needed for the database upload and the
         worksheer data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
      """

      self.type = ComponentType.OTsensor
      cdict = configuration.copy()

      # Use Hamamatsu as the default for Manufacturer and Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'Hamamatsu'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'Hamamatsu'

      # The class name is the name_label of the Wafer written in the txt file
      name = dreader.getDataFromTag('#ID')[:19]

      # Set kind_of_part and batch number according to the Wafer name label
      cdict['kind_of_part'] = self.kind_of_part(name)
      cdict['batch_number'] = self.batch_number(name)

      # Set the production date and the extended data
      objDate = datetime.strptime(dreader.getDataFromTag('#Date'),'%d-%b-%y')
      cdict['product_date'] = datetime.strftime(objDate,'%Y-%m-%d')

      BaseUploader.__init__(self, name, cdict, dreader)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                         'attributes']

      # initialize the envelope_ids
      self.envelope_ids = set()
      if envelope_ids!=None:
         data = envelope_ids.getDataAsCWiseVector()
         self.envelope_ids = sorted( { n[:19] for n in data[0] if n!='' } )


   def barcode(self):
      """Returns the list_of_ids."""
      return self.name

   def batch_number(self,barcode=None):
      """Return the batch number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode[0:5]

   def wafer_number(self,barcode=None):
      """Return the sensor number from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode[6:9]

   def wafer_type(self,barcode=None):
      """Return the wafer type from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode[10:13]

   def part_identifier(self,barcode=None):
      """Return the part identifier from the barcode."""
      if barcode==None:  barcode = self.name
      return barcode[14:19]

   def match(self,barcode):
      """Return True if the inout barcode matches the class name."""
      if self.batch_number()    == self.batch_number(barcode) and\
         self.wafer_number()    == self.wafer_number(barcode) and\
         self.wafer_type()      == self.wafer_type(barcode)   and\
         self.part_identifier() == self.part_identifier(barcode):
          return True
      return False

   def kind_of_part(self,barcode=None):
      """Return the kind_of_part to be associated to this barcode."""
      if barcode==None:  barcode = self.name
      typ = self.wafer_type(barcode)
      if   typ=='2-S':   return "2S Sensor"
      elif typ=='PSS':   return "PS-s Sensor"
      elif typ=='PSP':   return "PS-p Sensor"
      return None

   def dump_xml_data(self,filename='',wafers=None):
      """Writes the sensor component in the XML file for the database upload.
         It requires the name of the XML file to be produced. A SensorWafer
         class can also be input: in this case a parent-child relationship
         with the Sensor components is set (the wafer components must have
         been already entered in the database).
      """
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root_tree,"PARTS")

      self.xml_builder(parts,wafers)

      if filename == '':
         filename = '%s_sensors.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      wafers = args[0]

      children = parts
      if wafers is not None:
         for wafer in wafers:
            if wafer.match(self.barcode()):
               wafer_kop  = wafer.kind_of_part()
               wafer_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
               etree.SubElement(wafer_part,"BARCODE").text = wafer.barcode()
               children = etree.SubElement(wafer_part,"CHILDREN")

      batchN  = self.batch_number()
      waferN  = self.wafer_number()
      waferT  = self.wafer_type()
      partID  = self.part_identifier()
      name_label = '{}_{}_{}_{}'.format(batchN,waferN,waferT,partID)
      bcode      = '{}_{}_{}_{}'.format(batchN,waferN,waferT,partID)
      sid        = self.reader.getDataFromTag('#Scratch PAD')
      for eid in self.envelope_ids:
          if self.match(eid):   bcode = eid

      self.build_parts_on_xml(children,serial=sid,barcode=bcode,\
                              name_label=name_label)


class OTSensorMeasurements(ConditionData):
   """Produces the XML file for storing the sensor measurement data."""
   iv_data        = {'cond_name' : 'Tracker Strip-Sensor IV Test',
                     'table_name': 'TEST_SENSOR_IV',
                     'DBvar_vs_TxtHeader' : {'VOLTS'       : 'voltage [V]',
                                             'CURRNT_NAMP' : 'current [A]',
                                             'TEMP_DEGC'   : 'Not in txt file',
                                             'RH_PRCNT'    : 'Not in txt file'
                                            },
                     'mandatory' : ['VOLTS','CURRNT_NAMP']
                    }
   cv_data        = {'cond_name' : 'Tracker Strip-Sensor CV Test',
                     'table_name': 'TEST_SENSOR_CV',
                     'DBvar_vs_TxtHeader' : {'VOLTS'        : 'voltage [V]',
                                             'CAPCTNC_PFRD' : 'capacitance [F]',
                                             'TEMP_DEGC'    : 'Not in txt file',
                                             'RH_PRCNT'     : 'Not in txt file'
                                            },
                     'mandatory' : ['VOLTS','CAPCTNC_PFRD']
                    }
   bad_strip_data = {'cond_name' : 'OT Sensor Bad Strips',
                     'table_name': 'OTSENSOR_BADSTRIP',
                     'DBvar_vs_TxtHeader' : {},
                     'Strip_failure_type' : ['AC NG','AC open','AC short',\
                                             'DC high','DC low','DC leaky']
                    }
   summary_data   = {'cond_name' : 'Tracker Strip-Sensor Summary Data',
                     'table_name': 'TEST_SENSOR_SMMRY',
                     'DBvar_vs_TxtHeader' : {
                                 'AV_TEMP_DEGC'     : '#temperature [deg]',
                                 'AV_RH_PRCNT'      : '#humidity [%]',
                                 'FREQ_HZ'          : 'Not in txt file',
                                 'LCR_MODE'         : 'Not in txt file',
                                 'LCR_AMP_V'        : 'Not in txt file',
                                 'DEPLETION_VLTG_V' : '#Depletion voltage [V]',
                                 'CMPLNC_VLTG_IV_V' : 'Not in txt file',
                                 'CMPLNC_VLTG_CV_V' : 'Not in txt file'
                                            },
                     'mandatory' : ['AV_TEMP_DEGC','AV_RH_PRCNT']
                    }

   def __init__(self, run_conf, data_conf, data_type, dreader, sensor_data):
      """Constructor: it requires the description of the run data (run_conf),
         the description of the measurement data (data_conf), the type of data
         (data_type), the data table of the measurements (dreader) and the
         sensor data (sensor_data).

         Mandatory configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
            kind_of_part:    the type of component whose data belong to;
            serial:          identifier of the component whose data belong to;
            barcode:         identifier of the component whose data belong to;
            name_label:      identifier of the component whose data belong to;
            cond_name:       kind_of_condition name corresponding to these data;
            table_name:      condition data table where data have to be recorded;
            data_version:    data set version;
            DBV_vs_Theader:  describe how to associate data to the DB variables;
         run_name and run_type + run_number are exclusive. Serial, barcode and
         name_label are mutual exclusive.

         Optional parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            data_comment: description/comment on the dataset
            inserter:     person recording the run metadata (override the user)
      """

      # measurement whose value must be inverted for VQC run
      self.invert_value = ['VOLTS','CURRNT_NAMP']
      self.typ = data_type
      self.compliance_voltage = ''

      # The class name is the name_label of the Wafer written in the txt file
      sensor  = OTsensor({},sensor_data)
      batchN  = sensor.batch_number()
      waferN  = sensor.wafer_number()
      waferT  = sensor.wafer_type()
      partID  = sensor.part_identifier()
      name_label = '{}_{}_{}_{}'.format(batchN,waferN,waferT,partID)

      # Set kind_of_part and batch number according to the Wafer name label
      objDate = datetime.strptime(sensor_data.getDataFromTag('#Date'),'%d-%b-%y')
      run_metadata = run_conf.copy()
      run_metadata['run_begin']    = datetime.strftime(objDate,'%Y-%m-%d')
      run_metadata['kind_of_part'] = sensor.kind_of_part()
      run_metadata['name_label']   = name_label

      run_metadata['run_operator'] = sensor_data.getDataFromTag('#Operator')
      run_metadata['run_location'] = 'Hamamatsu'

      configuration = {}
      configuration.update(run_metadata)
      configuration.update(data_conf)
      if data_type==OTDataType.iv:
         name = 'IV-{}'.format(sensor.name)
         configuration.update(self.iv_data)
         ConditionData.__init__(self, name, configuration, dreader)
      if data_type==OTDataType.cv:
         name = 'CV-{}'.format(sensor.name)
         configuration.update(self.cv_data)
         ConditionData.__init__(self, name, configuration, dreader)
      if data_type==OTDataType.bad_strip:
         name = 'BAD_STRIP-{}'.format(sensor.name)
         configuration.update(self.bad_strip_data)
         ConditionData.__init__(self, name, configuration, sensor_data)
      if data_type==OTDataType.summary:
         name = 'SUMMARY-{}'.format(sensor.name)
         configuration.update(self.summary_data)
         ConditionData.__init__(self, name, configuration, sensor_data)

   def build_data_block(self,dataset):
      """Steers the building of the data block."""

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      if self.typ==OTDataType.iv:
         if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
                data_description['VOLTS'],data_description['CURRNT_NAMP']):
            raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
         self.build_table_measurements(dataset)
      if self.typ==OTDataType.cv:
         if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
                data_description['VOLTS'],data_description['CAPCTNC_PFRD']):
            raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
         self.build_table_measurements(dataset)
      if self.typ==OTDataType.bad_strip:
         self.build_bad_strip_block(dataset)
      if self.typ==OTDataType.summary:
         self.build_summary_measurements(dataset)

   def write_data_block(self):
      """Check if data block must be written."""
      if self.typ==OTDataType.bad_strip:
         bad_strips = 0
         try:
            bad_strips = int(self.reader.getDataFromTag('#Bad strips'))
         except:  pass
         if bad_strips==0: return False
      return True

   def build_bad_strip_block(self,dataset):
      """Build the block with the bad strip data."""

      bad_strip_nr = 0
      try:
         bad_strip_nr = int( self.reader.getDataFromTag('#Bad strips') )
      except:  pass

      if bad_strip_nr!=0:
         failures = self.retrieve('Strip_failure_type')
         number = 0
         comment = ''
         for f in failures:
            strips =  self.reader.getDataFromTag('#{}'.format(f))
            comment += '#{} {}\n'.format(f,strips)
            if strips!='none':
               for s in strips.split(','):
                  try:
                     strip_number = str( int(s) )
                     data = etree.SubElement(dataset,"DATA")
                     etree.SubElement(data,"KIND_OF_FAILURE_ID").text = f
                     etree.SubElement(data,"STRIP_NUMBER").text = strip_number
                     number += 1
                  except:  pass
         if bad_strip_nr!=number:
            raise BadStripInconsistency(self.__class__.__name__,bad_strip_nr,number,comment)


   def build_table_measurements(self,dataset):
      """Build the data from the measurements table."""

      table_data = self.reader.getDataAsCWiseDictRowSplit()
      # scale to nano-ampere and pico-farad
      if self.typ == OTDataType.iv:   scale2(table_data,'current [A]',1e9)
      if self.typ == OTDataType.cv:   scale2(table_data,'capacitance [F]',1e12)

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for i,point in enumerate(table_data):
         data_elements = []
         skip_record   = False
         for var in data_description.keys():
            try:
               value = point[data_description[var]]
               x = float(value)
               if var in self.invert_value:    x = -x if x!=0. else x
               data_elements.append((var,str(x)))
            except  KeyError:
               # key not present in data file, if not mandatory pass
               mandatory = self.retrieve('mandatory')
               if var in mandatory:
                  raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
               pass
            except ValueError:
               skip_record = True

         if not skip_record:
            data = etree.SubElement(dataset,"DATA")
            for el in data_elements:
               etree.SubElement(data,"{}".format(el[0])).text = el[1]


   def build_summary_measurements(self,dataset):
      """build_table_measurementsld the data from the measurements table."""

      data = etree.SubElement(dataset,"DATA")
      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for var in data_description.keys():
         value = self.reader.getDataFromTag(data_description[var])
         if value != None:
            etree.SubElement(data,"{}".format(var)).text = value
      for child in self.children:
         compliance_voltage = child.get_compliance_voltage()
         if child.typ==OTDataType.iv and compliance_voltage!=None:
            etree.SubElement(data,'CMPLNC_VLTG_IV_V').text = compliance_voltage
         if child.typ==OTDataType.cv and compliance_voltage!=None:
            etree.SubElement(data,'CMPLNC_VLTG_CV_V').text = compliance_voltage


   def get_compliance_voltage(self):
      """Check the data to see if compliance voltage is reached."""
      table_data = self.reader.getDataAsCWiseDictRowSplit()
      # scale to nano-ampere and pico-farad
      if self.typ == OTDataType.iv:   scale2(table_data,'current [A]',1e9)
      if self.typ == OTDataType.cv:   scale2(table_data,'capacitance [F]',1e12)

      data_description = self.retrieve('DBvar_vs_TxtHeader')
      for i,point in enumerate(table_data):
         data_elements = []
         for var in data_description.keys():
            try:
               value = point[data_description[var]]
               x = float(value)
               if var in self.invert_value:    x = -x if x!=0. else x
               data_elements.append((var,str(x)))
            except  KeyError:
               # key not present in data file, if not mandatory pass
               mandatory = self.retrieve('mandatory')
               if var in mandatory:
                  raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
               pass
            except ValueError:
               if self.verbose:
                  print('compliance reached in {}'.format(self.reader.filename))
               #skip_record = True
               last_data = data_elements[-1]
               if last_data[0]=='VOLTS':
                  self.compliance_voltage=last_data[1]
                  return self.compliance_voltage
      return None

class Wafer(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the Wafer
      component data read from a worksheet."""

   def __init__(self, name, cdict, dreader=None):
      """Constructor: it requires a name to tag the class (name), the dict for
         configuration (cdict) defining the ancillary data needed for the upload
         in the database and the worksheet data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
      """
      #Use Hamamatsu as default Manufacturer for Sensors
      if 'manufacturer' not in cdict.keys():
         cdict['manufacturer'] = 'Hamamatsu'

      BaseUploader.__init__(self, name, cdict, dreader)

      self.type = ComponentType.OTwafer

      self.mandatory += ['kind_of_part','locations','unique_location',\
                         'manufacturer','attributes']

      self.list_of_ids = set()

      #initialize the list_of_ids with the reader list
      if self.reader!=None:
         data = self.reader.getDataAsCWiseVector()
         self.list_of_ids = { n[:13] for n in data[0] if n!='' }


   def add(self, *args):
     """Add a new ID to the list_of_ids, assuming it is a legal barcode for
        OT wafer. Can digest either a single parameter or three parameters
        being the three fields for building the Wafer barcode. """
     if len(args)==1:
        id = args[0]
        self.list_of_ids.add( id[:13] )
     elif len(args)==3:
        batch_number = args[0]
        wafer_number = args[1]
        wafer_type   = args[2]
        id = '{:5s}_{:3s}_{:3s}'.format(batch_number,wafer_number,wafer_type)
        self.list_of_ids.add( id )

   def barcodes(self):
      """Returns the list_of_ids."""
      return sorted( list(self.list_of_ids) )

   def batch_number(self,barcode):
      """Return the batch number from the barcode."""
      return barcode[0:5]

   def wafer_number(self,barcode):
      """Return the sensor number from the barcode."""
      return barcode[6:9]

   def wafer_type(self,barcode):
      """Return the wafer type from the barcode."""
      return barcode[10:13]

   def kind_of_part(self,barcode):
      """Return the kind_of_part to be associated to this barcode."""
      typ = self.wafer_type(barcode)
      if   typ=='2-S':   return "2S Wafer"
      elif typ=='PSS':   return "PS-s Wafer"
      elif typ=='PSP':   return "PS-p Wafer"
      return None

   def match(self,barcode):
      """Return the barcode matching the one given in input."""
      barcodes = self.barcodes()
      for b in barcodes:
         if b in barcode:   return b
      return None


   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         

      self.xml_builder(root)

      if filename == '':
         filename = '{}_wafers.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


class WaferFromIDlist(Wafer):
   """Produces the XML file from the table with the initial test reasult."""

   def __init__(self, name, cdict, dreader=None):#, get_sensor_id=True):
      Wafer.__init__(self, name, cdict, dreader)#, get_sensor_id)
      #self.database = database

   def xml_builder(self, root_tree, *args):
      """Processes data in the reader."""
      parts = etree.SubElement(root_tree,"PARTS")
      barcodes = self.barcodes()
      for b in barcodes:
         if b!='':
            batchN  = self.batch_number(b)
            waferN  = self.wafer_number(b)
            waferT  = self.wafer_type(b)
            name_label = '{}_{}_{}'.format(batchN,waferN,waferT)
            self.update_configuration('kind_of_part',self.kind_of_part(b))
            self.update_configuration('batch_number',batchN)
            part_id = self.db.component_id(b,'barcode',self.kind_of_part())
            if part_id==None: self.build_parts_on_xml(parts,barcode=b,name_label=name_label)



class Sensor(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the Sensor
      component data read from a worksheet."""

   def __init__(self, name, cdict, dreader=None):
      """Constructor: it requires the class name (bname), the dict for
         configuration (cdict) defining the ancillary data needed for the
         database upload and the worksheer data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
      """

      #Use Hamamatsu as default Manufacturer for Sensors
      if 'manufacturer' not in cdict.keys():
         cdict['manufacturer'] = 'Hamamatsu-HPK'

      BaseUploader.__init__(self, name, cdict, dreader)

      self.type = ComponentType.OTsensor
      self.mandatory += ['kind_of_part','locations','unique_location',\
                         'manufacturer','attributes']

      self.list_of_ids = set()

      #initialize the list_of_ids with the reader list
      if self.reader!=None:
         data = self.reader.getDataAsCWiseVector()
         self.list_of_ids = { n for n in data[0] if len(n)==19 }


   def add(self, *args):
      """Add a new ID to the list_of_ids, assuming it is a legal barcode for
         OT sensor. Can digest either a single parameter or four parameters
         being the four fields for building the Sensor barcode."""
      if len(args)==1:
         id = args[0]
         self.list_of_ids.add( id[:19] )
      elif len(args)==3:
         batch_number = args[0]
         wafer_number = args[1]
         wafer_type   = args[2]
         part_type    = args[3]
         id = '{:5s}_{:3s}_{:3s}_{:5s}'.\
            format(batch_number,wafer_number,wafer_type,part_type)
         self.list_of_ids.add( id )

   def barcodes(self):
      """Returns the list_of_ids."""
      return sorted( list(self.list_of_ids) )

   def batch_number(self,barcode):
      """Return the batch number from the barcode."""
      return barcode[0:5]

   def wafer_number(self,barcode):
      """Return the sensor number from the barcode."""
      return barcode[6:9]

   def wafer_type(self,barcode):
      """Return the wafer type from the barcode."""
      return barcode[10:13]

   def part_identifier(self,barcode):
      """Return the part identifier from the barcode."""
      return barcode[14:19]

   def kind_of_part(self,barcode):
      """Return the kind_of_part to be associated to this barcode."""
      typ = self.wafer_type(barcode)
      if   typ=='2-S':   return "2S Sensor"
      elif typ=='PSS':   return "PS-s Sensor"
      elif typ=='PSP':   return "PS-p Sensor"
      return None

   def dump_xml_data(self,filename='',wafers=None):
     """Writes the sensor component in the XML file for the database upload.
        It requires the name of the XML file to be produced. A SensorWafer
        class can also be input: in this case a parent-child relationship
        with the Sensor components is set (the wafer components must have
        been already entered in the database).
     """
     inserter  = self.retrieve('inserter')
     root      = etree.Element("ROOT")
     if inserter is not None:
        root.set('operator_name',inserter)

     self.xml_builder(root,wafers)

     if filename == '':
        filename = '%s_sensors.xml'%self.name

     with open(filename, "w") as f:
        f.write( etree.tostring(root, pretty_print = True, \
                                      xml_declaration = True,\
                                      standalone="yes").decode('UTF-8') )


class SensorFromIiDlist(Sensor):
   """Produces the XML file from the table with the initial test reasult."""

   def __init__(self, name, cdict, dreader=None):
      Sensor.__init__(self, name, cdict, dreader)

   def xml_builder(self, root_tree, *args):
      """Processes data in the reader."""
      wafers = args[0]
      parts = etree.SubElement(root_tree,"PARTS")

      barcodes = self.barcodes()
      for bc in barcodes:
         children = parts
         if wafers is not None:
            wafer_bc = wafers.match(bc)
            if wafer_bc != None:
               wafer_kop  = wafers.kind_of_part(wafer_bc)
               wafer_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
               etree.SubElement(wafer_part,"BARCODE").text = wafer_bc
               children = etree.SubElement(wafer_part,"CHILDREN")

         batchN  = self.batch_number(bc)
         waferN  = self.wafer_number(bc)
         waferT  = self.wafer_type(bc)
         partID  = self.part_identifier(bc)
         name_label = '{}_{}_{}_{}'.format(batchN,waferN,waferT,partID)
         self.update_configuration('kind_of_part',self.kind_of_part(bc))
         self.update_configuration('batch_number',batchN)
         self.build_parts_on_xml(children,barcode=bc,name_label=name_label)



class SensorConditionData(BaseUploader):
   """Class to produce the xml file for the DBloader upload. It handles the
      Sensor condition data read from a worksheet (e.g. IV curve, CV, ...)."""

   def __init__(self, name, cdict, dreader, runp):
      """Constructor: it requires the name of the instance (name) the
         configuration (cdict), the data worksheet (dreader) and the run number
         provider (runp).
         Mandatory configuration:
            run_type:     type of condition run measurement;
            run_name:     name of condition run measurement;
            version:      the version of the measurement;
            table_name:   the name of the extension table;
            data_name:    the name of the measurement;
            var1_name:    the name of the first variable;
            var2_name:    the name of the second variable;
            var3_name:    the name of the third variable;
            var4_name:    the name of the fourth variable;
            kind_of_part: the part to which the measurement are related;
         run_type and run_name are exclusive.
      """

      BaseUploader.__init__(self, name, cdict, dreader)

      self.reader = dreader

      runp.setDbQuery('select r.run_number from trker_cmsr.trk_ot_test_nextrun_v r')
      self.runp = runp

      self.mandatory += ['run_type','run_name','version','table_name',\
                         'data_name','var1_name','var2_name','var3_name',\
                         'var4_name','kind_of_part','run_date']


   def dump_xml_data(self, sensors, run_d=None, filename=''):
      """Dump the sensor condition data in the XML file for the database upload. It
         requires in input the Sensor class providing the list of sensors for which
         the data have to be uploaded. A dictionary (run_d) that associates the run
         number with the sensor_id can optionally be provided to inhibit the run
         provider extraction. A filename for the XML file can also be entered.
      """

      runs = {}
      files = []
      table_data  = self.reader.getDataAsCWiseDict()

      for barcode in sensors.barcodes():
         part_name = sensors.part_identifier(barcode)
         wafer_number = '{}'.format(int(sensors.wafer_number(barcode)))

         # skip if a sensor is not listed in sensors
         if part_name not in self.reader.sheetname:  continue

         # skip sensor missing in excel table
         if wafer_number not in table_data.keys():
            print( '{} not present in excel table!'.format(barcode) )
            continue

         # set run number and fill it in the run dictionary
         run = None
         try:     run = run_d[barcode]
         except:  run = self.runp.run_number()
         runs[barcode] = run

         # initialise the XML file and create the body
         inserter  = self.retrieve('inserter')
         root      = etree.Element("ROOT")
         if inserter is not None:
            root.set('operator_name',inserter)
         
         self.update_configuration('kind_of_part',sensors.kind_of_part(barcode))
         self.xml_builder(root,table_data,wafer_number,run,barcode)

         # write the XML file
         filename = '{}_{}_condition.xml'.format(barcode,self.name)
         with open(filename, "w") as f:
            f.write( etree.tostring(root, pretty_print = True, \
                                          xml_declaration = True,\
                                          standalone="yes").decode('UTF-8') )
         files.append(filename)

      from zipfile import ZipFile
      zipfilename = '{}_condition.zip'.format(self.name)
      with ZipFile(zipfilename, 'w') as (zip):
         for f in files: zip.write(f)
      for f in files:  os.remove(f)
      return runs


class SensorTest(SensorConditionData):
   """Produces the XML file from the table with the Sensor Initial Test results."""

   def __init__(self, name, cdict, dreader, runp ):
      SensorConditionData.__init__(self, name, cdict, dreader,runp)

      self.invert_value = ['VOLTS','CURRNT_NAMP']


   def xml_builder(self,root_tree,*args):
      """Process data in the reader."""
      table_data   = args[0]
      wafer_number = args[1]
      run          = args[2]
      barcode      = args[3]

      run_date = self.retrieve('run_date')
      date = run_date if run_date!=None else strftime("%Y-%m-%d", gmtime())

      description = self.retrieve('description')

      run_operator = self.retrieve('run_operator')
      run_location = self.retrieve('run_location')
      run_comment  = self.retrieve('run_comment')
      self.build_header_on_xml( root_tree, run_location, run_operator,\
                                run_comment, run, run_begin=date )


      dataset = etree.SubElement(root_tree,"DATA_SET")
      etree.SubElement(dataset,'COMMENT_DESCRIPTION').text = \
                                               self.retrieve('description')

      etree.SubElement(dataset,'VERSION').text = self.retrieve('version')

      part = etree.SubElement(dataset,"PART")
      etree.SubElement(part,"KIND_OF_PART").text=\
                                               self.retrieve('kind_of_part')

      etree.SubElement(part,"BARCODE").text = barcode

      var1_str = self.retrieve('var1_name')
      var2_str = self.retrieve('var2_name')

      for counter,point in enumerate(table_data['VR']):
         try:
            #float(table_data[wafer_number][counter])
            x = float(point)
            y = float(table_data[wafer_number][counter])
            if var1_str in self.invert_value:    x = -x if x!=0. else x
            if var2_str in self.invert_value:    y = -y if y!=0. else y
            data = etree.SubElement(dataset,"DATA")
            etree.SubElement(data,var1_str).text = str(x)
            etree.SubElement(data,var2_str).text = str(y)
            # ancillary variables not set in the excel file
            z = None
            k = None
            if z is not None:
               etree.SubElement(data,self.retrieve('var3_name')).text = str(z)
            if k is not None:
               etree.SubElement(data,self.retrieve('var4_name')).text = str(k)
         except:
            #skip x-y pair if no float value is put on Y
            continue


class UpdateId(BaseUploader):
   """Class to update the id of the Components."""

   def __init__(self, name, cdict ):
      """Constructor: it requires the class name (name) and the configuration
         dict (cdict) defining the ancillary data needed for the database upload.
      """

      BaseUploader.__init__(self, name, cdict, None)

      self.mandatory += ['kind_of_part']


   def dump_xml_data(self, components, filename=''):
      """Writes the XML file for the database upload. It requires in input
         the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      parts = etree.SubElement(root,"PARTS")

      self.update_configuration('kind_of_part',components.retrieve('kind_of_part'))

      try:
         from rhapi import RhApi
      except:
         print( 'Cannot import rhapi: resthub package not in the path.' )
         print( 'Download the resthub package and run bin/setup.sh' )
         sys.exit(1)

      dburl = 'https://cmsdca.cern.ch/trk_rhapi'
      DBapi = RhApi(dburl, False, sso='login')

      barcodes = components.barcodes()
      for barcode in barcodes:
         batchN  = components.batch_number(barcode)
         waferN  = components.wafer_number(barcode)
         waferT  = components.wafer_type(barcode)
         query   = 'select p.id from trker_cmsr.parts p where p.barcode=\'%s\'' % barcode
         DBapi.clean(DBapi.qid(query) )
         Qres  = DBapi.json2(query)
         part_id = str( Qres['data'][0]['id'] )
         print ('barcode {} has part_id={}'.format(barcode,part_id))

         name_label = ''
         if components.type==ComponentType.OTsensor:
            partID  = components.part_identifier(barcode)
            name_label = '{}_{}_{}_{}'.format(batchN,waferN,waferT,partID)
         if components.type==ComponentType.OTwafer:
            name_label = '{}_{}_{}'.format(batchN,waferN,waferT)

         self.update_parts_on_xml(parts, part_id, '', barcode, name_label)

      if filename == '':
         filename = '{}.xml'.format(self.name)
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

class Halfmoon(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the Halfmoon
      component data."""

   def __init__(self, name, cdict, dreader=None):
      """Constructor: it requires the class name (bname), the dict for
         configuration (cdict) defining the ancillary data needed for the
         database upload and the worksheer data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
         The kind_of_part is desumed from the Halfmoon id.
      """

      #Use Hamamatsu as default Manufacturer for Sensors
      if 'manufacturer' not in cdict.keys():
         cdict['manufacturer'] = 'Hamamatsu-HPK'

      BaseUploader.__init__(self, name, cdict, dreader)

      self.type = ComponentType.OTsensor
      self.mandatory += ['kind_of_part','locations','unique_location',\
                         'manufacturer','attributes']

      #self.database = database
      self.list_of_ids = set()

      #initialize the list_of_ids with the reader list
      if self.reader!=None:
         data = self.reader.getDataAsCWiseVector()
         for d in data[0] :
            barcode = d.strip().split()[0]
            type    = self.wafer_type(barcode)
            part    = self.part_identifier(barcode)
            is_cut  = len(d.strip().split())!=1
            code    = barcode[:14]
            if part=='HM_XX':
               if type=='2-S':
                  self.add(code+'HM_EE')
                  self.add(code+'HM_NN')
                  self.add(code+'HM_WW')
                  if is_cut:
                     self.add(code+'HM_SS_OLDTS')
                     self.add(code+'HM_SS_DIODES_1')
                     self.add(code+'HM_SS_DIODES_2')
                     self.add(code+'HM_BABY_SENSOR_2S')
                  else:
                     self.add(code+'HM_SS')
               else:
                  self.add(code+'HM_EE')
                  self.add(code+'HM_NE')
                  self.add(code+'HM_NW')
                  self.add(code+'HM_WW')
                  self.add(code+'HM_SE')
                  if is_cut:
                     self.add(code+'HM_SW_BRIL')
                     self.add(code+'HM_SW_DIODES_1')
                     self.add(code+'HM_SW_DIODES_2')
                     if type=='PSS':
                        self.add(code+'HM_BABY_SENSOR_PSS')
                     else:
                        self.add(code+'HM_BABY_SENSOR_PSP')
                  else:
                     self.add(code+'HM_SW')
            else:
               self.add(barcode)

         #self.list_of_ids  = { n.strip().split()[0] for n in data[0] }
         #self.list_of_cuts = { n.strip().split()[0] for n in data[0] if len(n.strip().split())!=1}

   def add(self, *args):
      """Add a new ID to the list_of_ids, assuming it is a legal barcode for
         OT Halfmoon. Can digest either a single parameter or four parameters
         being the four fields for building the Halfmoon barcode."""
      if len(args)==1:
         id = args[0]
         self.list_of_ids.add( id )
      elif len(args)==3:
         batch_number = args[0]
         wafer_number = args[1]
         wafer_type   = args[2]
         part_type    = args[3]
         id = '{:5s}_{:3s}_{:3s}_{:15s}'.\
            format(batch_number,wafer_number,wafer_type,part_type)
         self.list_of_ids.add( id )


   def barcodes(self):
      """Returns the list_of_ids."""
      return sorted( list(self.list_of_ids) )


   def batch_number(self,barcode):
      """Return the batch number from the barcode."""
      return barcode[0:5]

   def wafer_number(self,barcode):
      """Return the sensor number from the barcode."""
      return barcode[6:9]

   def wafer_type(self,barcode):
      """Return the wafer type from the barcode."""
      return barcode[10:13]

   def part_identifier(self,barcode):
      """Return the part identifier from the barcode."""
      return barcode[14:]

   def kind_of_part(self,barcode):
      """Return the kind_of_part to be associated to this barcode."""
      typ = self.wafer_type(barcode)
      prt = self.part_identifier(barcode)
      int = ''
      if   typ=='2-S':   int = '2S '
      elif typ=='PSS':   int = 'PS-s '
      elif typ=='PSP':   int = 'PS-p '

      if   prt=='HM_EE':   return int+'Halfmoon E'
      elif prt=='HM_WW':   return int+'Halfmoon W'
      elif prt=='HM_NN':   return int+'Halfmoon N'
      elif prt=='HM_NE':   return int+'Halfmoon NE'
      elif prt=='HM_NW':   return int+'Halfmoon NW'
      elif prt=='HM_SE':   return int+'Halfmoon SE'
      elif 'HM_SS' in prt or ('BABY_SENSOR' in prt and typ=='2-S'):
         return int+'Halfmoon S'
      elif 'HM_SW' in prt or ('BABY_SENSOR' in prt and typ!='2-S'):
         return int+'Halfmoon SW'
      return None

   def dump_xml_data(self,filename='',wafers=None):
     """Writes the sensor component in the XML file for the database upload.
        Assumes that Halfmoon are children of the Wafers, so it always set the
        parent-child relationship with the parent wafer. If the wafer has not
        been inserted in the databse it creates an XML file to upload it (to
        be uploaded before the halfmoon).
     """
     inserter  = self.retrieve('inserter')
     root      = etree.Element("ROOT")
     if inserter is not None:
        root.set('operator_name',inserter)

     w_conf = {}
     w_conf['description']     = 'Wafer inserted for Halfmoon production.'
     w_conf['product_date']    = self.configuration['product_date']
     w_conf['manufacturer']    = self.configuration['manufacturer']
     w_conf['unique_location'] = self.configuration['unique_location']
     w_conf['attributes']      = [('Status','Good')]

     if 'inserter' in self.configuration.keys():
        w_conf['inserter']     = self.configuration['inserter']

     atts = self.configuration['attributes']
     for att in atts:
        if att[0]=='Sensor Type': w_conf['attributes'].append(att)

     wafers = WaferFromIDlist(self.name,w_conf,self.reader)
     wafer_list = { '{}_{}_{}'.format(wafers.batch_number(c),\
                     wafers.wafer_number(c), wafers.wafer_type(c))\
                     for c in self.barcodes() }

     #for w in sorted( list(wafer_list) ):
    #    if self.verbose:  print( "Checking if wafer {} is in DB".format(w) )
    #    if not self.database.component_id(w,kop=self.kind_of_part()): wafers.add(w)
     wafers.dump_xml_data()


     self.xml_builder(root,wafers)

     if filename == '':
        filename = '%s_halfmoon.xml'%self.name

     with open(filename, "w") as f:
        f.write( etree.tostring(root, pretty_print = True, \
                                      xml_declaration = True,\
                                      standalone="yes").decode('UTF-8') )

class HalfmoonFromIDlist(Halfmoon):
   """Produces the XML file from list of barcodes."""

   def __init__(self, name, cdict, dreader=None):
      Halfmoon.__init__(self, name, cdict, dreader)

   def xml_builder(self, root_tree , *args):
      """Processes data in the reader."""
      wafers = args[0]
      parts = etree.SubElement(root_tree,"PARTS")

      barcodes = self.barcodes()
      for bc in barcodes:
         # children = parts
         # Halfmoons are always children of wafers
         wafer_nl = '{}_{}_{}'.format(wafers.batch_number(bc),
                                      wafers.wafer_number(bc),
                                      wafers.wafer_type(bc))
         wafer_kop  = wafers.kind_of_part(wafer_nl)
         wafer_part = etree.SubElement(parts, "PART", mode="auto")
         etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
         etree.SubElement(wafer_part,"NAME_LABEL").text = wafer_nl
         children = etree.SubElement(wafer_part,"CHILDREN")

         current_attr = self.configuration['attributes']
         for att in current_attr:
            if 'Halfmoon Structure' in att[0]:
               self.configuration['attributes'].remove(att)

         batchN  = self.batch_number(bc)
         waferN  = self.wafer_number(bc)
         waferT  = self.wafer_type(bc)
         partID  = self.part_identifier(bc)

         ky = ''
         if waferT=='2-S': ky='2S Halfmoon Structure'
         else:             ky='PS Halfmoon Structure'

         att = ()
         if   'OLDTS'       in partID:  att = (ky,'OLDTS')
         elif 'BRIL'        in partID:  att = (ky,'BRIL')
         elif 'DIODES_1'    in partID:  att = (ky,'DIODES 1')
         elif 'DIODES_2'    in partID:  att = (ky,'DIODES 2')
         elif 'BABY_SENSOR' in partID:  att = (ky,'BABY SENSOR')
         else:  att = (ky,'Full')

         self.configuration['attributes'].append(att)

         name_label = '{}_{}_{}_{}'.format(batchN,waferN,waferT,partID)
         self.update_configuration('kind_of_part',self.kind_of_part(bc))
         self.update_configuration('batch_number',batchN)
         self.build_parts_on_xml(children,barcode=bc,name_label=name_label)



class HalfmoonMeasurements(ConditionData):
  """Produces the XML file for storing the sensor measurement data."""
  iv_data = {'cond_name'  : 'Tracker Halfmoon IV Test',
             'table_name' : 'TEST_SENSOR_IV',
             'DBvar_vs_TxtHeader' : {'VOLTS'            : 'VOLTS',
                                     'CURRNT_NAMP'      : 'CURRNT_NAMP',
                                     'TEMP_DEGC'        : 'TEMP_DEGC',
                                     'RH_PRCNT'         : 'RH_PRCNT',
                                     'BIASCURRNT_NAMPR' : 'BIASCURRNT_NAMPR',
                                     'TIME'             : 'TIME',
                                     'AIR_TEMP_DEGC'    : 'AIR_TEMP_DEGC'
                                    },
              'mandatory' : ['VOLTS','CURRNT_NAMP']
            }

  cv_data = {'cond_name'  : 'Tracker Halfmoon CV Test',
             'table_name' : 'TEST_SENSOR_CV',
             'DBvar_vs_TxtHeader' : {'VOLTS'            : 'VOLTS',
                                     'CAPCTNC_PFRD'     : 'CAPCTNC_PFRD',
                                     'TEMP_DEGC'        : 'TEMP_DEGC',
                                     'RH_PRCNT'         : 'RH_PRCNT',
                                     'BIASCURRNT_NAMPR' : 'BIASCURRNT_NAMPR',
                                     'TIME'             : 'TIME',
                                     'AIR_TEMP_DEGC'    : 'AIR_TEMP_DEGC',
                                     'RESSTNC_MOHM'     : 'RESSTNC_MOHM'
                                    },
              'mandatory' : ['VOLTS','CAPCTNC_PFRD']
            }

  dcv_data = {'cond_name'  : 'Tracker Halfmoon Diode CV Test',
              'table_name' : 'TEST_DIODE_CV',
              'DBvar_vs_TxtHeader' : {'DQRTR_VOLTS' : 'DQRTR_VOLTS',
                                      'DQRTR_CAPCTNC_PFRD' : 'DQRTR_CAPCTNC_PFRD',
                                      'DQRTR_TEMP_DEGC' : 'DQRTR_TEMP_DEGC',
                                      'DQRTR_RH_PRCNT'  : 'DQRTR_RH_PRCNT',
                                      'DQRTR_BIASCURRNT_NAMPR' : 'DQRTR_BIASCURRNT_NAMPR',
                                      'DQRTR_TIME' : 'DQRTR_TIME',
                                      'DQRTR_AIR_TEMP_DEGC' : 'DQRTR_AIR_TEMP_DEGC',
                                      'DQRTR_RESSTNC_MOHM' : 'DQRTR_RESSTNC_MOHM',
                                      'DHLF_VOLTS' : 'DHLF_VOLTS',
                                      'DHLF_CAPCTNC_PFRD' : 'DHLF_CAPCTNC_PFRD',
                                      'DHLF_TEMP_DEGC' : 'DHLF_TEMP_DEGC',
                                      'DHLF_RH_PRCNT' : 'DHLF_RH_PRCNT',
                                      'DHLF_BIASCURRNT_NAMPR' : 'DHLF_BIASCURRNT_NAMPR',
                                      'DHLF_TIME' : 'DHLF_TIME',
                                      'DHLF_AIR_TEMP_DEGC' : 'DHLF_AIR_TEMP_DEGC',
                                      'DHLF_RESSTNC_MOHM' : 'DHLF_RESSTNC_MOHM',
                                      'PLANAR_CAPCTNC_PFRD' : 'PLANAR_CAPCTNC_PFRD',
                                      'EDGE_CAPCTNC_PFRD' : 'EDGE_CAPCTNC_PFRD'
                                     },
              'mandatory' : ['DQRTR_VOLTS','DQRTR_CAPCTNC_PFRD','DHLF_VOLTS',\
                             'DHLF_CAPCTNC_PFRD']
             }

  igv_data = {'cond_name'  : 'Tracker Halfmoon IGV Test',
              'table_name' : 'TEST_SENSOR_IV',
              'DBvar_vs_TxtHeader' : {'VOLTS'            : 'VOLTS',
                                      'CURRNT_NAMP'      : 'CURRNT_NAMP',
                                      'TEMP_DEGC'        : 'TEMP_DEGC',
                                      'RH_PRCNT'         : 'RH_PRCNT',
                                      'BIASCURRNT_NAMPR' : 'BIASCURRNT_NAMPR',
                                      'TIME'             : 'TIME',
                                      'AIR_TEMP_DEGC'    : 'AIR_TEMP_DEGC'
                                     },
              'mandatory' : ['VOLTS','CURRNT_NAMP']
              }

  tc_data = {'cond_name'  : 'Tracker Halfmoon TC Test',
             'table_name' : 'TEST_SENSOR_IV',
             'DBvar_vs_TxtHeader' : {'VOLTS'            : 'VOLTS',
                                     'CURRNT_NAMP'      : 'CURRNT_NAMP',
                                     'TEMP_DEGC'        : 'TEMP_DEGC',
                                     'RH_PRCNT'         : 'RH_PRCNT',
                                     'BIASCURRNT_NAMPR' : 'BIASCURRNT_NAMPR',
                                     'TIME'             : 'TIME',
                                     'AIR_TEMP_DEGC'    : 'AIR_TEMP_DEGC'
                                     },
             'mandatory' : ['VOLTS','CURRNT_NAMP']
            }

  mm_data = {'cond_name'  : 'Tracker Halfmoon Mask Misalignment',
             'table_name' : 'TEST_MASK_MISALIGNMENT',
             'DBvar_vs_TxtHeader' : { 'PICTURE_BLOB'    : 'PICTURE_BLOB',
                                      'PICTURE_FILE'    : 'PICTURE_FILE',
                                      'PICTURE_DATE'    : 'PICTURE_DATE',
                                      'L1_STRIP_H1'     : 'L1_STRIP_H1',
                                      'L2_STRIP_H1'     : 'L2_STRIP_H1',
                                      'L1_STRIP_H2'     : 'L1_STRIP_H2',
                                      'L2_STRIP_H2'     : 'L2_STRIP_H2',
                                      'L1_STOP_H'       : 'L1_STOP_H',
                                      'L2_STOP_H'       : 'L2_STOP_H',
                                      'L1_POLY_H'       : 'L1_POLY_H',
                                      'L2_POLY_H'       : 'L2_POLY_H',
                                      'L1_STRIP_V1'     : 'L1_STRIP_V1',
                                      'L2_STRIP_V1'     : 'L2_STRIP_V1',
                                      'L1_STRIP_V2'     : 'L1_STRIP_V2',
                                      'L2_STRIP_V2'     : 'L2_STRIP_V2',
                                      'L1_STOP_V'       : 'L1_STOP_V',
                                      'L2_STOP_V'       : 'L2_STOP_V',
                                      'L1_POLY_V'       : 'L1_POLY_V',
                                      'L2_POLY_V'       : 'L2_POLY_V',
                                      'L1_PASS_METAL_V' : 'L1_PASS_METAL_V',
                                      'L2_PASS_METAL_V' : 'L2_PASS_METAL_V',
                                      'L1_PASS_METAL_H' : 'L1_PASS_METAL_H',
                                      'L2_PASS_METAL_H' : 'L2_PASS_METAL_H',
                                      'L1_PASS_STRIP_V' : 'L1_PASS_STRIP_V',
                                      'L2_PASS_STRIP_V' : 'L2_PASS_STRIP_V',
                                      'L1_PASS_STRIP_H' : 'L1_PASS_STRIP_H',
                                      'L2_PASS_STRIP_H' : 'L2_PASS_STRIP_H',
                                      'L1_CONTACT_V'    : 'L1_CONTACT_V',
                                      'L2_CONTACT_V'    : 'L2_CONTACT_V',
                                      'L1_CONTACT_H'    : 'L1_CONTACT_H',
                                      'L2_CONTACT_H'    : 'L2_CONTACT_H',
                                      'L1_EDGE_V'       : 'L1_EDGE_V'
                                   },
             'mandatory' : []
            }

  mp_data = {'cond_name'  : 'Tracker Halfmoon Mask Parameneters',
             'table_name' : 'MASK_MISALIGNMENT_PAR',
             'DBvar_vs_TxtHeader' : { 'DELTA_L_STRIP_H1'     : 'DELTA_L_STRIP_H1',
                                      'DELTA_L_STOP_H'       : 'DELTA_L_STOP_H',
                                      'DELTA_L_STRIP_H2'     : 'DELTA_L_STRIP_H2',
                                      'DELTA_L_POLY_H'       : 'DELTA_L_POLY_H',
                                      'DELTA_L_STRIP_V1'     : 'DELTA_L_STRIP_V1',
                                      'DELTA_L_STOP_V'       : 'DELTA_L_STOP_V',
                                      'DELTA_L_STRIP_V2'     : 'DELTA_L_STRIP_V2',
                                      'DELTA_L_POLY_V'       : 'DELTA_L_POLY_V',
                                      'DELTA_L_PASS_METAL_V' : 'DELTA_L_PASS_METAL_V',
                                      'DELTA_L_PASS_STRIP_V' : 'DELTA_L_PASS_STRIP_V',
                                      'DELTA_L_CONTACT_V'    : 'DELTA_L_CONTACT_V',
                                      'DELTA_L_EDGE_V'       : 'DELTA_L_EDGE_V',
                                      'DELTA_L_PASS_METAL_H' : 'DELTA_L_PASS_METAL_H',
                                      'DELTA_L_PASS_STRIP_H' : 'DELTA_L_PASS_STRIP_H',
                                      'DELTA_L_CONTACT_H'    : 'DELTA_L_CONTACT_H',
                                      'DELTA_L_EDGE_H'       : 'DELTA_L_EDGE_H'
                                     },
             'mandatory' : []
            }

  hm_meta = {'cond_name'  : 'Tracker Halfmoon Metadata',
             'table_name' : 'HALFMOON_METADATA',
             'DBvar_vs_TxtHeader' : { 'KIND_OF_HM_FLUTE_ID' : 'KIND_OF_HM_FLUTE_ID',
                                      'KIND_OF_HM_STRUCT_ID' : 'KIND_OF_HM_STRUCT_ID'
                                     },
             'hm_flute_type' : ['PQC1','PQC2','PQC3','PQC4','RINT','RBULKMETAL',\
                                'RSTRIPPOLY','REDGEMETAL','RESISTIVITY','DIEL', \
                                'FET','CAP','OVAL_FET_2S','OVAL_FET_PSS','MASK'],
             'hm_struct_type': ['CAP_E','CAP_W','CAP_LARGE_E','CAP_LARGE_W',\
                                'CAP_MED_E','CAP_MED_W','CAP_SMALL_E','CAP_SMALL_W',\
                                'CBKR_POLY','CBKR_STRIP','CC_EDGE','CC_POLY',\
                                'CC_STRIP','CLOVER_METAL','DIEL_NE','DIEL_NW',\
                                'DIEL_SW','DIEL4','DIEL5','DIEL6','DIEL7',\
                                'DIEL8','DIEL9','DIEL10','DIEL15','DIEL16',\
                                'DIEL17','DIEL18','DIODE_HALF','DIODE_QUARTER',\
                                'FET_HGCAL_ATOLL','FET_HGCAL_COM','FET_PSS',\
                                'FET_2S','FET80','FET60','FET40','FOUR_POINT_LARGE',\
                                'FOUR_POINT_MED_E','FOUR_POINT_MED_W','FOUR_POINT_SMALL_E',\
                                'FOUR_POINT_SMALL_W','GCD','GCD05','LINEWIDTH_EDGE',\
                                'LINEWIDTH_METAL','LINEWIDTH_POLY','LINEWIDTH_STOP',\
                                'LINEWIDTH_STRIP','MASK_MISALIGNMENT','MEANDER_METAL',\
                                'MOS_QUARTER','OVAL_FET_PSS','OVAL_FET_PSS_NOSTOP',\
                                'OVAL_FET_2S','OVAL_FET_2S_NOSTOP','R_POLY','VDP_BULK',\
                                'VDP_BULK_E','VDP_BULK_W','VDP_EDGE','VDP_METAL',\
                                'VDP_POLY','VDP_STOP','VDP_STRIP'],
           'mandatory' : []
          }

  def __init__(self, run_conf, data_conf, \
               halfmoon_id=None, dreader=None, data_uploader=None):
     """Constructor: it requires the name label of the halfmoon (halfmoon_id),
        the flute type (flute), the structure type (structure), the description
        of the run data (run_conf) and the description of the measurement data
        (data_conf).

        Mandatory configuration parameters (run / data) are:
           run_name:        name of condition run measurement;
           run_type:        type of condition run measurement;
           run_number:      run number to be associated to the run type
           run_begin:       timestamp of the begin of run
           kind_of_part:    the type of component whose data belong to;
           name_label:      identifier of the component whose data belong to;
           cond_name:       kind_of_condition name corresponding to these data;
           table_name:      condition data table where data have to be recorded;
           data_version:    data set version;
           DBV_vs_Theader:  describe how to associate data to the DB variables;
        run_name and run_type + run_number are exclusive. Serial, barcode and
        name_label are mutual exclusive.

        Optional parameters are:
           run_end:      timestamp of the end of run
           run_operator: operator that performed the run
           run_location: site where the run was executed
           run_comment:  description/comment on the run
           data_comment: description/comment on the dataset
           inserter:     person recording the run metadata (override the user)
     """
     # Check consistency
     from os import path as path

     name = ''
     configuration = {}

     if data_uploader!=None:
        # These are metadata
        self.typ = HMDataType.METADATA
        self.halfmoon_id    = data_uploader.halfmoon_id
        self.structure_id   = data_uploader.structure()
        self.flute_id       = data_uploader.flute()
        name = '{}-{}-{}'.format(self.halfmoon_id,self.flute_id,self.structure_id)
        configuration.update({'flute_id':self.flute_id,'structure_id':self.structure_id})
     else:
        # These are measurement data
        name = path.splitext( path.basename(dreader.filename) )[0]
        data_type = name.split('-')[2]

        if data_type not in HMDataType._member_names_ :
           raise NotRecognized(self.__class__.__name__,\
                '{} --> {}'.format(dreader.filename,data_type),'as data type.')
        if halfmoon_id==None:
           raise BadInputParameter(self.__class__.__name__,'halfmoon_id',halfmoon_id)

        self.typ = HMDataType[data_type]
        self.halfmoon_id = halfmoon_id


     # Set kind_of_part and batch number according to the Wafer name label
     #objDate = datetime.strptime(sensor_data.getDataFromTag('#Date'),'%d-%b-%y')
     #configuration = {}
     configuration.update(run_conf)
     configuration.update(data_conf)
     configuration['kind_of_part'] = self.kind_of_part(self.halfmoon_id)
     configuration['name_label']   = self.halfmoon_id

     if self.typ is HMDataType.IV:       configuration.update(self.iv_data)
     if self.typ is HMDataType.CV:       configuration.update(self.cv_data)
     if self.typ is HMDataType.DIODECV:  configuration.update(self.dcv_data)
     if self.typ is HMDataType.IGV:      configuration.update(self.igv_data)
     if self.typ is HMDataType.TC:       configuration.update(self.tc_data)
     if self.typ is HMDataType.MM:       configuration.update(self.mm_data)
     if self.typ is HMDataType.MMP:      configuration.update(self.mp_data)
     if self.typ is HMDataType.METADATA: configuration.update(self.hm_meta)

     ConditionData.__init__(self, name, configuration, dreader)

     # Final check on flute and structure types
     if self.flute() not in self.hm_meta['hm_flute_type']:
        raise NotRecognized(self.__class__.__name__,self.flute(),'as flute type.')
     if self.structure() not in self.hm_meta['hm_struct_type']:
        raise NotRecognized(self.__class__.__name__,self.structure(),'as structure type.')



  def flute(self):
     """Return the flute Id which the data belongs to."""
     if self.reader != None:
        from os import path as path
        data_name = path.splitext( path.basename(self.reader.filename) )[0]
        return data_name.split('-')[0]
     else:
        return self.flute_id

  def structure(self):
     """Return the structure Id which the data belongs to."""
     if self.reader != None:
        from os import path as path
        data_name = path.splitext( path.basename(self.reader.filename) )[0]
        return data_name.split('-')[1]
     else:
        return self.structure_id

  def build_data_block(self,dataset):
     """Steers the building of the data block."""
     data_description = self.retrieve('DBvar_vs_TxtHeader')
     if self.typ is HMDataType.IV or self.typ is HMDataType.IGV or self.typ is HMDataType.TC:
        #if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
        #       data_description['VOLTS'],data_description['CURRNT_NAMP']):
        #   raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
        self.build_table_measurements(dataset)
     if self.typ is HMDataType.CV:
        #if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
        #       data_description['VOLTS'],data_description['CAPCTNC_PFRD']):
        #   raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
        self.build_table_measurements(dataset)
     if self.typ is HMDataType.DIODECV:
        #if not consistency_check(self.reader.getDataAsCWiseDictRowSplit(),\
        #       data_description['DQRTR_VOLTS'],\
        #       data_description['DQRTR_CAPCTNC_PFRD'],\
        #       data_description['DHLF_VOLTS'],\
        #       data_description['DHLF_CAPCTNC_PFRD']):
         #  raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
        self.build_table_measurements(dataset)
     if self.typ is HMDataType.MM:
        self.build_table_measurements(dataset)
     if self.typ is HMDataType.MMP:
        self.build_table_measurements(dataset)
     if self.typ is HMDataType.METADATA:
        self.build_metadata(dataset)

  def write_data_block(self):
     """Check if data block must be written."""
     return True


  def build_table_measurements(self,dataset):
     """Build the data from the measurements table."""
     table_data       = self.reader.getDataAsCWiseDictRowSplit()
     data_description = self.retrieve('DBvar_vs_TxtHeader')
     for i,point in enumerate(table_data):
        data = etree.SubElement(dataset,"DATA")
        for var in data_description.keys():
           try:
              value = point[data_description[var]]
              etree.SubElement(data,"{}".format(var)).text = value
              #values.append()
           except KeyError:
              # key not present in data file, if not mandatory pass
              mandatory = self.retrieve('mandatory')
              if var in mandatory:
                 raise BadMeasurement(self.__class__.__name__,self.name,self.typ)
              pass
           except:
              print(self.reader.filename)
              sys.exit(1)


  def build_metadata(self,dataset):
     """build the metadata from inetranal configuration."""
     data = etree.SubElement(dataset,"DATA")
     etree.SubElement(data,'KIND_OF_HM_FLUTE_ID').text = self.flute()
     etree.SubElement(data,'KIND_OF_HM_STRUCT_ID').text = self.structure()


  def wafer_type(self,barcode):
     """Return the wafer type from the barcode."""
     return barcode[10:13]


  def part_identifier(self,barcode):
     """Return the part identifier from the barcode."""
     return barcode[14:]


  def kind_of_part(self,barcode):
     """Return the kind_of_part to be associated to this barcode."""
     typ = self.wafer_type(barcode)
     prt = self.part_identifier(barcode)
     int = ''
     if   typ=='2-S':   int = '2S '
     elif typ=='PSS':   int = 'PS-s '
     elif typ=='PSP':   int = 'PS-p '

     if   prt=='HM_EE':   return int+'Halfmoon E'
     elif prt=='HM_WW':   return int+'Halfmoon W'
     elif prt=='HM_NN':   return int+'Halfmoon N'
     elif prt=='HM_NE':   return int+'Halfmoon NE'
     elif prt=='HM_NW':   return int+'Halfmoon NW'
     elif prt=='HM_SE':   return int+'Halfmoon SE'
     elif 'HM_SS' in prt or ('BABY_SENSOR' in prt and typ=='2-S'):
        return int+'Halfmoon S'
     elif 'HM_SW' in prt or ('BABY_SENSOR' in prt and typ!='2-S'):
        return int+'Halfmoon SW'
     return None
